﻿using HotChocolate;
using HotChocolate.Resolvers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoneApi.Models;
using VoneApi.Services;

namespace VoneApi.Resolver
{
    public class BlogResolver
    {
        private readonly IBlogInterface _blogInterface;

        public BlogResolver([Service] IBlogInterface blogInterface)
        {
            _blogInterface = blogInterface;
        }

        public IEnumerable<Blog> GetBlogs(Blog blog, IResolverContext ctx)
        {
            return _blogInterface.GetAll().Where(b => b.Title.Equals(blog.Title));
        }
    }
}
