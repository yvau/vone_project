﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoneApi.BindingModels;
using VoneApi.Models;

namespace VoneApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Property, PropertyModel>();
            // CreateMap<PaginationQuery, PaginationFilter>();
            CreateMap<Blog, BlogModelView>();
        }
    }
}
