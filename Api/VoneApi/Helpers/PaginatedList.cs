﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoneApi.Helpers
{
    public class PaginatedList<T> : List<T>
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; private set; }

        /*public PaginatedList(IEnumerable<T> data)
        {
            Data = data;
        }*/

        public IEnumerable<T> Data { get; set; }

        /*public PaginatedList(List<T> items, int count, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);

            this.AddRange(items);
        }*/

        public bool HasPreviousPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }

        public static async Task<PaginatedList<T>> CreateAsync(IQueryable<T> source, int pageIndex, int pageSize)
        {
            var count = await source.CountAsync();
            var items = await source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
            // return new PaginatedList<T>(items, count, pageIndex, pageSize);
            return new PaginatedList<T>
            {
                Data = items,
                PageIndex = pageIndex,
                TotalPages = (int)Math.Ceiling(count / (double)pageSize)
                // this.AddRange(items)
                // PageNumber = pagination.PageNumber >= 1 ? pagination.PageNumber : (int?)null,
                // PageSize = pagination.PageSize >= 1 ? pagination.PageSize : (int?)null,
                // NextPage = response.Any() ? nextPage : null,
                // PreviousPage = previousPage
            };
        }
    }
}
