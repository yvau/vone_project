﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoneApi.Services;

namespace VoneApi.Helpers
{
    public class PaginationHelpers
    {

        public static PagedResponse<T> CreatePaginatedResponse<T>(IQueryable<T> source, int pageIndex, int pageSize, UtilService _utilService) 
        {
            var items = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            var nextPage = pageIndex >= 1
               ? _utilService.GetAllPostsUri(new PaginationQuery(pageIndex + 1, pageSize)).ToString()
               : null;

            var previousPage = pageIndex - 1 >= 1
                ? _utilService.GetAllPostsUri(new PaginationQuery(pageIndex - 1, pageSize)).ToString()
                : null;

            return new PagedResponse<T>
            {
                Data = items,
                PageNumber = pageIndex,
                PageSize = pageSize,
                NextPage = items.Any() ? nextPage : null,
                PreviousPage = previousPage
            };
        }
    }
}
