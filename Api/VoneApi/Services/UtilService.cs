using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Caching.Distributed;
using VoneApi.Data;
using VoneApi.Models;
using VoneApi.BindingModels;
using VoneApi.Helpers;
using Microsoft.AspNetCore.WebUtilities;

namespace VoneApi.Services
{
    public interface IUtilInterface
    {
        ApiResponse DisplayErrors(IList<ValidationFailure> Errors);

        long GetId(String resources);

        void SetStringValue(string key, string value, int? expireTime = null);

        String GetStringValue(string key);

        Uri GetPostUri(string postId);

        Uri GetAllPostsUri(PaginationQuery pagination = null);
    }

    public class UtilService : IUtilInterface
    {
        private readonly ApplicationDbContext _context;
        private readonly IDistributedCache _distributedCache;
        private string _baseUri = "https://localhost:5001";

        public UtilService(ApplicationDbContext context, IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
            _context = context;     
        }

        // display errors 
        public ApiResponse DisplayErrors(IList<ValidationFailure> Errors)
        {
            // init list of ErrorModel
            IDictionary<string, string[]> errors = new Dictionary<string, string[]>();

            // assign value to the ErrorModel for each distinct propertyname
            foreach (var failure in Errors.GroupBy(o => o.PropertyName).ToList())
            {
                errors.Add(failure.Select(o => o.PropertyName).First(), failure.Select(o => o.ErrorMessage).ToArray());
            }

            return new ApiResponse(false, errors);
        }


        // manual increment id
        public long GetId(String resource)
        {
            // get the autoincrement object based on the parameter {resource}
            var autoIncrement = _context.AutoIncrement.FirstOrDefault(m => m.Id == resource);

            // assign 0 to the incrementNumber to return
            long incrementNumber = 0;

            // if autoincrement doesn't exist create the data
            if (autoIncrement == null)
            {
                AutoIncrement incrementIfNull = new AutoIncrement { Id = resource, IncrementNumber = ++incrementNumber };
                _context.Add(incrementIfNull);
                _context.SaveChanges();
            }

            // if autoincrement doesn't exist update the data add +1 to the IncrementNumber
            else
            {
                incrementNumber = autoIncrement.IncrementNumber.Value;
                autoIncrement.Id = resource;
                autoIncrement.IncrementNumber = ++incrementNumber;

                _context.Update(autoIncrement);
                _context.SaveChanges();
            }

            return incrementNumber;
        }

        public void SetStringValue(string key, string value, int? expireTime = null)
        {
            DistributedCacheEntryOptions cacheOptions = new DistributedCacheEntryOptions();
            if (expireTime.HasValue)
                // Sets the time the cache will expire
                cacheOptions.SetAbsoluteExpiration(TimeSpan.FromMinutes(expireTime.Value));
                // Sets how long the cache can be inactive before being removed
                // opcoesCache.SetSlidingExpiration(TimeSpan.FromMinutes(2));
            else
                // Sets the time the cache will expire
                cacheOptions.SetAbsoluteExpiration(TimeSpan.FromMinutes(10));
                // Sets how long the cache can be inactive before being removed
                // opcoesCache.SetSlidingExpiration(TimeSpan.FromMinutes(2));


            _distributedCache.SetString(key, value, cacheOptions);
        }

        public String GetStringValue(string key)
        {
            return _distributedCache.GetString(key);
        }

        public Uri GetPostUri(string postId)
        {
            return new Uri(_baseUri);
        }

        public Uri GetAllPostsUri(PaginationQuery pagination = null)
        {
            var uri = new Uri(_baseUri);

            if (pagination == null)
            {
                return uri;
            }

            var modifiedUri = QueryHelpers.AddQueryString(_baseUri, "pageNumber", pagination.PageNumber.ToString());
            modifiedUri = QueryHelpers.AddQueryString(modifiedUri, "pageSize", pagination.PageSize.ToString());

            return new Uri(modifiedUri);
        }

    }
}
