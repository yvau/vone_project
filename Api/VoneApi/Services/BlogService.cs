﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoneApi.Data;
using VoneApi.Models;

namespace VoneApi.Services
{
    public interface IBlogInterface
    {
        IQueryable<Blog> GetAll();

        Task SaveAsync(Blog blog);

        Task<Blog> ShowAsync(long id);

        Task<Blog> UpdateAsync(Blog blog);

        Task<Blog> DeleteAsync(Blog blog);
    }

    public class BlogService : IBlogInterface
    {
        private readonly ApplicationDbContext _context;

        public BlogService(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<Blog> GetAll()
        {
            return _context.Blog.AsQueryable();
        }

        // save blog
        public async Task SaveAsync(Blog blog)
        {
            _context.Add(blog);
            await _context.SaveChangesAsync();
        }

        // find profile by Email
        public async Task<Blog> ShowAsync(long id)
        {
            return await _context.Blog.FindAsync(id);
        }

        public async Task<Blog> UpdateAsync(Blog blog)
        {
            _context.Update(blog);
            await _context.SaveChangesAsync();

            return blog;
        }

        public async Task<Blog> DeleteAsync(Blog blog)
        {
            _context.Blog.Remove(blog);
            await _context.SaveChangesAsync();

            return blog;
        }
    }
}
