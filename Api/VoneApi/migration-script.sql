﻿CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" character varying(150) NOT NULL,
    "ProductVersion" character varying(32) NOT NULL,
    CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);


DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE "AspNetRoles" (
        "Id" text NOT NULL,
        "Name" character varying(256) NULL,
        "NormalizedName" character varying(256) NULL,
        "ConcurrencyStamp" text NULL,
        CONSTRAINT "PK_AspNetRoles" PRIMARY KEY ("Id")
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE "AspNetUsers" (
        "Id" text NOT NULL,
        "UserName" character varying(256) NULL,
        "NormalizedUserName" character varying(256) NULL,
        "Email" character varying(256) NULL,
        "NormalizedEmail" character varying(256) NULL,
        "EmailConfirmed" boolean NOT NULL,
        "PasswordHash" text NULL,
        "SecurityStamp" text NULL,
        "ConcurrencyStamp" text NULL,
        "PhoneNumber" text NULL,
        "PhoneNumberConfirmed" boolean NOT NULL,
        "TwoFactorEnabled" boolean NOT NULL,
        "LockoutEnd" timestamp with time zone NULL,
        "LockoutEnabled" boolean NOT NULL,
        "AccessFailedCount" integer NOT NULL,
        "Avatar" text NULL,
        "FirstName" text NULL,
        "LastName" text NULL,
        "Gender" text NULL,
        "DateOfBirth" timestamp without time zone NOT NULL,
        "Biography" text NULL,
        "Language" text NULL,
        "FacebookHandler" text NULL,
        "GoogleHandler" text NULL,
        "CreatedAt" timestamp without time zone NOT NULL,
        "UpdatedAt" timestamp without time zone NOT NULL,
        CONSTRAINT "PK_AspNetUsers" PRIMARY KEY ("Id")
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE auto_increment (
        id character varying(150) NOT NULL,
        increment_number bigint NULL,
        CONSTRAINT "PK_auto_increment" PRIMARY KEY (id)
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE country (
        id character varying(150) NOT NULL,
        continent character varying(150) NULL,
        currency_code character varying(150) NULL,
        currency_name character varying(150) NULL,
        languages character varying(150) NULL,
        name character varying(150) NULL,
        phone_code character varying(150) NULL,
        postal_code character varying(150) NULL,
        postal_format character varying(150) NULL,
        tld character varying(150) NULL,
        CONSTRAINT "PK_country" PRIMARY KEY (id)
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE "AspNetRoleClaims" (
        "Id" integer NOT NULL GENERATED BY DEFAULT AS IDENTITY,
        "RoleId" text NOT NULL,
        "ClaimType" text NULL,
        "ClaimValue" text NULL,
        CONSTRAINT "PK_AspNetRoleClaims" PRIMARY KEY ("Id"),
        CONSTRAINT "FK_AspNetRoleClaims_AspNetRoles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "AspNetRoles" ("Id") ON DELETE CASCADE
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE appointment (
        id bigint NOT NULL,
        appointment_type integer NULL,
        start_date timestamp without time zone NULL,
        end_date timestamp without time zone NULL,
        caption character varying(150) NULL,
        description text NULL,
        location character varying(150) NULL,
        label integer NULL,
        status integer NULL,
        all_day boolean NULL,
        recurrence character varying(150) NULL,
        created_at timestamp without time zone NULL,
        updated_at timestamp without time zone NULL,
        aspnetusers_id character varying(450) NOT NULL,
        CONSTRAINT "PK_appointment" PRIMARY KEY (id),
        CONSTRAINT fk_appointment_aspnetusers1 FOREIGN KEY (aspnetusers_id) REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE "AspNetUserClaims" (
        "Id" integer NOT NULL GENERATED BY DEFAULT AS IDENTITY,
        "UserId" text NOT NULL,
        "ClaimType" text NULL,
        "ClaimValue" text NULL,
        CONSTRAINT "PK_AspNetUserClaims" PRIMARY KEY ("Id"),
        CONSTRAINT "FK_AspNetUserClaims_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers" ("Id") ON DELETE CASCADE
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE "AspNetUserLogins" (
        "LoginProvider" text NOT NULL,
        "ProviderKey" text NOT NULL,
        "ProviderDisplayName" text NULL,
        "UserId" text NOT NULL,
        CONSTRAINT "PK_AspNetUserLogins" PRIMARY KEY ("LoginProvider", "ProviderKey"),
        CONSTRAINT "FK_AspNetUserLogins_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers" ("Id") ON DELETE CASCADE
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE "AspNetUserRoles" (
        "UserId" text NOT NULL,
        "RoleId" text NOT NULL,
        CONSTRAINT "PK_AspNetUserRoles" PRIMARY KEY ("UserId", "RoleId"),
        CONSTRAINT "FK_AspNetUserRoles_AspNetRoles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "AspNetRoles" ("Id") ON DELETE CASCADE,
        CONSTRAINT "FK_AspNetUserRoles_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers" ("Id") ON DELETE CASCADE
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE "AspNetUserTokens" (
        "UserId" text NOT NULL,
        "LoginProvider" text NOT NULL,
        "Name" text NOT NULL,
        "Value" text NULL,
        CONSTRAINT "PK_AspNetUserTokens" PRIMARY KEY ("UserId", "LoginProvider", "Name"),
        CONSTRAINT "FK_AspNetUserTokens_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers" ("Id") ON DELETE CASCADE
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE blog (
        id bigint NOT NULL,
        title character varying(150) NULL,
        title_slug character varying(150) NULL,
        body text NULL,
        is_published boolean NULL,
        image_preview_url character varying(150) NULL,
        created_at timestamp without time zone NULL,
        updated_at timestamp without time zone NULL,
        tags character varying(150) NULL,
        enabled boolean NULL,
        aspnetusers_id character varying(450) NOT NULL,
        CONSTRAINT "PK_blog" PRIMARY KEY (id),
        CONSTRAINT fk_blog_aspnetusers1 FOREIGN KEY (aspnetusers_id) REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE request_proposal (
        id bigint NOT NULL,
        bathrooms integer NULL,
        bedrooms integer NULL,
        size numeric NULL,
        number_of_parking integer NULL,
        number_of_garage integer NULL,
        type_of_proposal character varying(150) NULL,
        price_minimum numeric NULL,
        price_maximum numeric NULL,
        is_currently_owner boolean NULL,
        is_there_contingency boolean NULL,
        is_with_pool character varying(150) NULL,
        is_pre_approved character varying(150) NULL,
        banking_institution character varying(150) NULL,
        urgency character varying(150) NULL,
        is_enabled character varying(150) NULL,
        is_published character varying(150) NULL,
        is_first_buyer character varying(150) NULL,
        is_furnished character varying(150) NULL,
        elevator character varying(150) NULL,
        wheelchair character varying(150) NULL,
        nearby_school character varying(150) NULL,
        nearby_parc character varying(150) NULL,
        nearby_sports_center character varying(150) NULL,
        nearby_trade character varying(150) NULL,
        nearby_public_transport character varying(150) NULL,
        nearby_waterfront character varying(150) NULL,
        nearby_navigable_water_body character varying(150) NULL,
        type_of_properties character varying(8000) NULL,
        status character varying(150) NULL,
        age_of_property character varying(150) NULL,
        created_at timestamp without time zone NULL,
        updated_at timestamp without time zone NULL,
        aspnetusers_id character varying(450) NOT NULL,
        CONSTRAINT "PK_request_proposal" PRIMARY KEY (id),
        CONSTRAINT fk_request_proposal_aspnetusers1 FOREIGN KEY (aspnetusers_id) REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE response_proposal (
        id bigint NOT NULL,
        type_of_proposal character varying(150) NULL,
        type_of_properties character varying(8000) NULL,
        price_minimum numeric NULL,
        price_maximum numeric NULL,
        created_at timestamp without time zone NULL,
        updated_at timestamp without time zone NULL,
        aspnetusers_id character varying(450) NOT NULL,
        CONSTRAINT "PK_response_proposal" PRIMARY KEY (id),
        CONSTRAINT fk_response_proposal_aspnetusers1 FOREIGN KEY (aspnetusers_id) REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE province (
        id character varying(150) NOT NULL,
        name character varying(150) NULL,
        name_ascii character varying(150) NULL,
        country_id character varying(150) NULL,
        CONSTRAINT "PK_province" PRIMARY KEY (id),
        CONSTRAINT fk_province_country1 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE bookmark_proposal (
        request_proposal_id bigint NOT NULL,
        aspnetusers_id character varying(450) NOT NULL,
        created_at timestamp without time zone NULL,
        CONSTRAINT bookmark_proposal_pkey PRIMARY KEY (request_proposal_id, aspnetusers_id),
        CONSTRAINT fk_bookmark_proposal_aspnetusers1 FOREIGN KEY (aspnetusers_id) REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT,
        CONSTRAINT fk_bookmark_proposal_request_proposal1 FOREIGN KEY (request_proposal_id) REFERENCES request_proposal (id) ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE city (
        id bigint NOT NULL,
        alternate_names text NULL,
        f_code character varying(150) NULL,
        latitude character varying(150) NULL,
        longitude character varying(150) NULL,
        name character varying(150) NULL,
        name_ascii character varying(150) NULL,
        timezone character varying(150) NULL,
        province_id character varying(150) NOT NULL,
        CONSTRAINT "PK_city" PRIMARY KEY (id),
        CONSTRAINT fk_cities_province1 FOREIGN KEY (province_id) REFERENCES province (id) ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE location (
        id bigint NOT NULL,
        postal_code character varying(45) NULL,
        street character varying(150) NULL,
        country_id character varying(150) NOT NULL,
        province_id character varying(150) NOT NULL,
        city_id bigint NOT NULL,
        CONSTRAINT "PK_location" PRIMARY KEY (id),
        CONSTRAINT fk_location_city1 FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE RESTRICT,
        CONSTRAINT fk_location_country1 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE RESTRICT,
        CONSTRAINT fk_location_province1 FOREIGN KEY (province_id) REFERENCES province (id) ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE location_has_proposal (
        id bigint NOT NULL,
        location_id bigint NOT NULL,
        request_proposal_id bigint NOT NULL,
        CONSTRAINT "PK_location_has_proposal" PRIMARY KEY (id),
        CONSTRAINT fk_location_has_proposal_location1 FOREIGN KEY (location_id) REFERENCES location (id) ON DELETE RESTRICT,
        CONSTRAINT fk_location_has_proposal_request_proposal1 FOREIGN KEY (request_proposal_id) REFERENCES request_proposal (id) ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE location_has_response (
        id bigint NOT NULL,
        location_id bigint NOT NULL,
        response_proposal_id bigint NOT NULL,
        CONSTRAINT "PK_location_has_response" PRIMARY KEY (id),
        CONSTRAINT fk_location_has_response_proposal_location1 FOREIGN KEY (location_id) REFERENCES location (id) ON DELETE RESTRICT,
        CONSTRAINT fk_location_has_response_response_proposal1 FOREIGN KEY (response_proposal_id) REFERENCES response_proposal (id) ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE property (
        id bigint NOT NULL,
        type character varying(150) NULL,
        sale_type character varying(150) NULL,
        description text NULL,
        price numeric NULL,
        bedrooms character varying(150) NULL,
        bathrooms character varying(150) NULL,
        year_built bigint NULL,
        characteristics text NULL,
        status character varying(150) NULL,
        size character varying(150) NULL,
        created_at timestamp without time zone NULL,
        updated_at timestamp without time zone NULL,
        location_id bigint NOT NULL,
        aspnetusers_id character varying(450) NOT NULL,
        CONSTRAINT "PK_property" PRIMARY KEY (id),
        CONSTRAINT fk_property_aspnetusers1 FOREIGN KEY (aspnetusers_id) REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT,
        CONSTRAINT fk_property_location1 FOREIGN KEY (location_id) REFERENCES location (id) ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE bookmark_property (
        property_id bigint NOT NULL,
        aspnetusers_id character varying(450) NOT NULL,
        created_at timestamp without time zone NULL,
        CONSTRAINT bookmark_property_pkey PRIMARY KEY (property_id, aspnetusers_id),
        CONSTRAINT fk_bookmark_property_aspnetusers1 FOREIGN KEY (aspnetusers_id) REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT,
        CONSTRAINT fk_bookmark_property_property1 FOREIGN KEY (property_id) REFERENCES property (id) ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE property_photo (
        id bigint NOT NULL,
        content_type character varying(150) NULL,
        created_at timestamp without time zone NULL,
        name character varying(150) NULL,
        size numeric NULL,
        url character varying(150) NULL,
        property_id bigint NOT NULL,
        CONSTRAINT "PK_property_photo" PRIMARY KEY (id),
        CONSTRAINT fk_property_photo_property1 FOREIGN KEY (property_id) REFERENCES property (id) ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE request_has_response (
        id bigint NOT NULL,
        property_id bigint NULL,
        created_at timestamp without time zone NULL,
        appointment_date timestamp without time zone NULL,
        status character varying(150) NULL,
        request_proposal_id bigint NULL,
        appointment_id bigint NULL,
        response_proposal_id bigint NOT NULL,
        CONSTRAINT "PK_request_has_response" PRIMARY KEY (id),
        CONSTRAINT fk_request_has_response_appointment1 FOREIGN KEY (appointment_id) REFERENCES appointment (id) ON DELETE RESTRICT,
        CONSTRAINT fk_proposal_has_proposal_property1 FOREIGN KEY (property_id) REFERENCES property (id) ON DELETE RESTRICT,
        CONSTRAINT fk_request_has_response_request_proposal1 FOREIGN KEY (request_proposal_id) REFERENCES request_proposal (id) ON DELETE RESTRICT,
        CONSTRAINT fk_request_has_response_response_proposal1 FOREIGN KEY (response_proposal_id) REFERENCES response_proposal (id) ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE TABLE request_has_response_satus_log (
        id bigint NOT NULL,
        request_has_response_satus_log_id bigint NOT NULL,
        status character varying(150) NULL,
        verb character varying(150) NULL,
        created_at timestamp without time zone NULL,
        request_has_response_id bigint NOT NULL,
        aspnetusers_id character varying(450) NOT NULL,
        aspnetusers_id_recipient character varying(450) NOT NULL,
        CONSTRAINT "PK_request_has_response_satus_log" PRIMARY KEY (id),
        CONSTRAINT fk_request_has_response_satus_log_aspnetusers1 FOREIGN KEY (aspnetusers_id) REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT,
        CONSTRAINT fk_request_has_response_satus_log_aspnetusers2 FOREIGN KEY (aspnetusers_id_recipient) REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT,
        CONSTRAINT fk_request_has_response_satus_log_request_has_response1 FOREIGN KEY (request_has_response_id) REFERENCES request_has_response (id) ON DELETE RESTRICT,
        CONSTRAINT fk_request_has_response_satus_log_request_has_response_satus_1 FOREIGN KEY (request_has_response_satus_log_id) REFERENCES request_has_response_satus_log (id) ON DELETE RESTRICT
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_appointment_aspnetusers1_idx ON appointment (aspnetusers_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX "IX_AspNetRoleClaims_RoleId" ON "AspNetRoleClaims" ("RoleId");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE UNIQUE INDEX "RoleNameIndex" ON "AspNetRoles" ("NormalizedName");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX "IX_AspNetUserClaims_UserId" ON "AspNetUserClaims" ("UserId");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX "IX_AspNetUserLogins_UserId" ON "AspNetUserLogins" ("UserId");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX "IX_AspNetUserRoles_RoleId" ON "AspNetUserRoles" ("RoleId");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX "EmailIndex" ON "AspNetUsers" ("NormalizedEmail");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE UNIQUE INDEX "UserNameIndex" ON "AspNetUsers" ("NormalizedUserName");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_blog_aspnetusers1_idx ON blog (aspnetusers_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_bookmark_property_aspnetusers1_idx ON bookmark_property (aspnetusers_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_bookmark_property_property1_idx ON bookmark_property (property_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_bookmark_proposal_aspnetusers1_idx ON bookmark_proposal (aspnetusers_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_bookmark_proposal_request_proposal1_idx ON bookmark_proposal (request_proposal_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_cities_province1_idx ON city (province_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_location_city1_idx ON location (city_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_location_country1_idx ON location (country_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_location_province1_idx ON location (province_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_location_has_proposal_location1_idx ON location_has_proposal (location_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_location_has_proposal_request_proposal1_idx ON location_has_proposal (request_proposal_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_location_has_response_proposal_location1_idx ON location_has_response (location_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_location_has_response_response_proposal1_idx ON location_has_response (response_proposal_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_property_aspnetusers1_idx ON property (aspnetusers_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_property_location1_idx ON property (location_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_property_photo_property1_idx ON property_photo (property_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_province_country1_id ON province (country_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_request_has_response_appointment1_idx ON request_has_response (appointment_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_proposal_has_proposal_property1_idx ON request_has_response (property_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_request_has_response_request_proposal1_idx ON request_has_response (request_proposal_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_request_has_response_response_proposal1_idx ON request_has_response (response_proposal_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_request_has_response_satus_log_aspnetusers1_idx ON request_has_response_satus_log (aspnetusers_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_request_has_response_satus_log_aspnetusers2_idx ON request_has_response_satus_log (aspnetusers_id_recipient);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_request_has_response_satus_log_request_has_response1_idx ON request_has_response_satus_log (request_has_response_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_request_has_response_satus_log_request_has_response_satu_idx ON request_has_response_satus_log (request_has_response_satus_log_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_request_proposal_aspnetusers1_idx ON request_proposal (aspnetusers_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    CREATE INDEX fk_response_proposal_aspnetusers1_idx ON response_proposal (aspnetusers_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20201112163635_InitialCreate') THEN
    INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
    VALUES ('20201112163635_InitialCreate', '3.1.9');
    END IF;
END $$;
