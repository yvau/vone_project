﻿using System;
using System.Collections.Generic;

namespace VoneApi.Models
{
    public partial class AutoIncrement
    {
        public string Id { get; set; }
        public long? IncrementNumber { get; set; }
    }
}
