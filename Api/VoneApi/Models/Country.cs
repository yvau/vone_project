﻿using System;
using System.Collections.Generic;

namespace VoneApi.Models
{
    public partial class Country
    {
        public Country()
        {
            Location = new HashSet<Location>();
            Province = new HashSet<Province>();
        }

        public string Id { get; set; }
        public string Continent { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string Languages { get; set; }
        public string Name { get; set; }
        public string PhoneCode { get; set; }
        public string PostalCode { get; set; }
        public string PostalFormat { get; set; }
        public string Tld { get; set; }

        public virtual ICollection<Location> Location { get; set; }
        public virtual ICollection<Province> Province { get; set; }
    }
}
