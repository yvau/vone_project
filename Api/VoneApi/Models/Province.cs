﻿using System;
using System.Collections.Generic;

namespace VoneApi.Models
{
    public partial class Province
    {
        public Province()
        {
            City = new HashSet<City>();
            Location = new HashSet<Location>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string NameAscii { get; set; }
        public string CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<City> City { get; set; }
        public virtual ICollection<Location> Location { get; set; }
    }
}
