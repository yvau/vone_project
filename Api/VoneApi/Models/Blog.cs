﻿using System;
using System.Collections.Generic;

namespace VoneApi.Models
{
    public partial class Blog
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string TitleSlug { get; set; }
        public string Body { get; set; }
        public bool? IsPublished { get; set; }
        public string ImagePreviewUrl { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string Tags { get; set; }
        public bool? Enabled { get; set; }
        public string AspnetusersId { get; set; }

        public virtual ApplicationUser Aspnetusers { get; set; }
    }
}
