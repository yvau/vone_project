﻿using System;
using System.Collections.Generic;

namespace VoneApi.Models
{
    public partial class City
    {
        public City()
        {
            Location = new HashSet<Location>();
        }

        public long Id { get; set; }
        public string AlternateNames { get; set; }
        public string FCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Name { get; set; }
        public string NameAscii { get; set; }
        public string Timezone { get; set; }
        public string ProvinceId { get; set; }

        public virtual Province Province { get; set; }
        public virtual ICollection<Location> Location { get; set; }
    }
}
