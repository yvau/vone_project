﻿using System;
using System.Collections.Generic;

namespace VoneApi.Models
{
    public partial class Appointment
    {
        public Appointment()
        {
            RequestHasResponse = new HashSet<RequestHasResponse>();
        }

        public long Id { get; set; }
        public int? AppointmentType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public int? Label { get; set; }
        public int? Status { get; set; }
        public bool? AllDay { get; set; }
        public string Recurrence { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string AspnetusersId { get; set; }

        public virtual ApplicationUser Aspnetusers { get; set; }
        public virtual ICollection<RequestHasResponse> RequestHasResponse { get; set; }
    }
}
