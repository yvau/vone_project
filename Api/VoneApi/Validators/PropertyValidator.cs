using FluentValidation;
using System;
using System.Collections.Generic;
using VoneApi.BindingModels;

namespace VoneApi.Validators
{
    public class PropertyValidator : AbstractValidator<PropertyModel>
    {

        public PropertyValidator()
        {

            /*RuleFor(propertyModelView => propertyModelView.Location.Name)
                .NotEmpty();*/

            RuleFor(propertyModel => propertyModel.Type)
               .EmailAddress().NotEmpty();

            RuleFor(propertyModel => propertyModel.Size)
                .EmailAddress().NotEmpty();

            RuleFor(propertyModel => propertyModel.Description)
                .NotEmpty();

            /*RuleFor(proposalModelView => proposalModelView.TypeOfProperties)
                .NotEmpty();

            RuleFor(proposalModelView => proposalModelView.Location)
                .Must(isCollectionNull)
                .WithMessage(locationResource.GetLocalizedHtmlString("EmailDontExist"));

             RuleFor(proposalModelView => proposalModelView.PriceMinimum)
                .LessThanOrEqualTo(proposalModelView => proposalModelView.PriceMaximum)
                .When(proposalModelView => !String.IsNullOrWhiteSpace(proposalModelView.PriceMaximum.ToString())); */
        }

        /*private Boolean isCollectionNull(ICollection<City> locationCollection)
        {
            if (locationCollection.Count > 0)
            {
                return true;
            }
            return false;
        }*/
    }
}
