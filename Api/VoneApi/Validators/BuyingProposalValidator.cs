﻿using FluentValidation;
using System;
using System.Linq;
using VoneApi.BindingModels;

namespace VoneApi.Validation
{
    public class BuyingProposalValidator : AbstractValidator<RequestProposalModelView>
    {
        public BuyingProposalValidator()
        {
            RuleFor(buyingProposal => buyingProposal.TypeOfProperties)
                .NotEmpty();
        }
    }
}
