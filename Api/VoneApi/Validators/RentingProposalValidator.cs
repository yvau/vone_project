﻿using FluentValidation;
using System;
using System.Linq;
using VoneApi.BindingModels;

namespace VoneApi.Validation
{
    public class RentingProposalValidator : AbstractValidator<RequestProposalModelView>
    {
        public RentingProposalValidator()
        {
            RuleFor(rentingProposal => rentingProposal.TypeOfProperties)
                .NotEmpty();
        }
    }
}
