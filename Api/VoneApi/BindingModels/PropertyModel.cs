﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System;
using System.ComponentModel.DataAnnotations;

namespace VoneApi.BindingModels
{
    public class PropertyModel
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string SaleType { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public string Bedrooms { get; set; }
        public string Bathrooms { get; set; }
        public long? YearBuilt { get; set; }
        public string Characteristics { get; set; }
        public string Status { get; set; }
        public string Size { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public long LocationId { get; set; }
        public string AspNetUsersId { get; set; }
    }
}
