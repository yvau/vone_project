﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoneApi.BindingModels
{
    public class BlogModelView
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string TitleSlug { get; set; }
        public string Body { get; set; }
        public bool? IsPublished { get; set; }
        public string ImagePreviewUrl { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string Category { get; set; }
        public bool? Enabled { get; set; }
        public string AspNetUsersId { get; set; }

    }
}
