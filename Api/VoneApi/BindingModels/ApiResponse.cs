﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoneApi.BindingModels
{
    public class ApiResponse
    {
        public ApiResponse(Boolean _success, Object _content)
        {
            Success = _success;
            Content = _content;
        }

        public Boolean Success { get; set; }
        public Object Content { get; set; }
    }
}
