﻿using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoneApi.Models;
using VoneApi.Resolver;

namespace VoneApi.Type
{
    public class BlogType : ObjectType<Blog>
    {
        protected override void Configure(IObjectTypeDescriptor<Blog> descriptor)
        {
            descriptor.Field(a => a.Id).Type<IdType>();
            descriptor.Field(a => a.Title).Type<StringType>();
            descriptor.Field(a => a.TitleSlug).Type<StringType>();
            descriptor.Field(a => a.Body).Type<StringType>();

            descriptor.Field<BlogResolver>(t => t.GetBlogs(default, default));
        }
    }
}
