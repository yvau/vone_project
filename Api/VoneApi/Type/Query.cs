﻿using HotChocolate.Types;
using HotChocolate.Types.Relay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoneApi.Models;
using VoneApi.Services;

namespace VoneApi.Type
{
    public class Query
    {
        private readonly IBlogInterface _blogInterface;

        public Query(IBlogInterface blogInterface)
        {
            _blogInterface = blogInterface;
        }

        [UsePaging(SchemaType = typeof(BlogType))]
        [UseFiltering]
        public IQueryable<Blog> blogs => _blogInterface.GetAll();
    }
}
