﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace VoneApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Avatar = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    Biography = table.Column<string>(nullable: true),
                    Language = table.Column<string>(nullable: true),
                    FacebookHandler = table.Column<string>(nullable: true),
                    GoogleHandler = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "auto_increment",
                columns: table => new
                {
                    id = table.Column<string>(maxLength: 150, nullable: false),
                    increment_number = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_auto_increment", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "country",
                columns: table => new
                {
                    id = table.Column<string>(maxLength: 150, nullable: false),
                    continent = table.Column<string>(maxLength: 150, nullable: true),
                    currency_code = table.Column<string>(maxLength: 150, nullable: true),
                    currency_name = table.Column<string>(maxLength: 150, nullable: true),
                    languages = table.Column<string>(maxLength: 150, nullable: true),
                    name = table.Column<string>(maxLength: 150, nullable: true),
                    phone_code = table.Column<string>(maxLength: 150, nullable: true),
                    postal_code = table.Column<string>(maxLength: 150, nullable: true),
                    postal_format = table.Column<string>(maxLength: 150, nullable: true),
                    tld = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_country", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "appointment",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    appointment_type = table.Column<int>(nullable: true),
                    start_date = table.Column<DateTime>(nullable: true),
                    end_date = table.Column<DateTime>(nullable: true),
                    caption = table.Column<string>(maxLength: 150, nullable: true),
                    description = table.Column<string>(nullable: true),
                    location = table.Column<string>(maxLength: 150, nullable: true),
                    label = table.Column<int>(nullable: true),
                    status = table.Column<int>(nullable: true),
                    all_day = table.Column<bool>(nullable: true),
                    recurrence = table.Column<string>(maxLength: 150, nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    aspnetusers_id = table.Column<string>(maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_appointment", x => x.id);
                    table.ForeignKey(
                        name: "fk_appointment_aspnetusers1",
                        column: x => x.aspnetusers_id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "blog",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    title = table.Column<string>(maxLength: 150, nullable: true),
                    title_slug = table.Column<string>(maxLength: 150, nullable: true),
                    body = table.Column<string>(nullable: true),
                    is_published = table.Column<bool>(nullable: true),
                    image_preview_url = table.Column<string>(maxLength: 150, nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    tags = table.Column<string>(maxLength: 150, nullable: true),
                    enabled = table.Column<bool>(nullable: true),
                    aspnetusers_id = table.Column<string>(maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_blog", x => x.id);
                    table.ForeignKey(
                        name: "fk_blog_aspnetusers1",
                        column: x => x.aspnetusers_id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "request_proposal",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    bathrooms = table.Column<int>(nullable: true),
                    bedrooms = table.Column<int>(nullable: true),
                    size = table.Column<decimal>(type: "numeric", nullable: true),
                    number_of_parking = table.Column<int>(nullable: true),
                    number_of_garage = table.Column<int>(nullable: true),
                    type_of_proposal = table.Column<string>(maxLength: 150, nullable: true),
                    price_minimum = table.Column<decimal>(type: "numeric", nullable: true),
                    price_maximum = table.Column<decimal>(type: "numeric", nullable: true),
                    is_currently_owner = table.Column<bool>(nullable: true),
                    is_there_contingency = table.Column<bool>(nullable: true),
                    is_with_pool = table.Column<string>(maxLength: 150, nullable: true),
                    is_pre_approved = table.Column<string>(maxLength: 150, nullable: true),
                    banking_institution = table.Column<string>(maxLength: 150, nullable: true),
                    urgency = table.Column<string>(maxLength: 150, nullable: true),
                    is_enabled = table.Column<string>(maxLength: 150, nullable: true),
                    is_published = table.Column<string>(maxLength: 150, nullable: true),
                    is_first_buyer = table.Column<string>(maxLength: 150, nullable: true),
                    is_furnished = table.Column<string>(maxLength: 150, nullable: true),
                    elevator = table.Column<string>(maxLength: 150, nullable: true),
                    wheelchair = table.Column<string>(maxLength: 150, nullable: true),
                    nearby_school = table.Column<string>(maxLength: 150, nullable: true),
                    nearby_parc = table.Column<string>(maxLength: 150, nullable: true),
                    nearby_sports_center = table.Column<string>(maxLength: 150, nullable: true),
                    nearby_trade = table.Column<string>(maxLength: 150, nullable: true),
                    nearby_public_transport = table.Column<string>(maxLength: 150, nullable: true),
                    nearby_waterfront = table.Column<string>(maxLength: 150, nullable: true),
                    nearby_navigable_water_body = table.Column<string>(maxLength: 150, nullable: true),
                    type_of_properties = table.Column<string>(maxLength: 8000, nullable: true),
                    status = table.Column<string>(maxLength: 150, nullable: true),
                    age_of_property = table.Column<string>(maxLength: 150, nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    aspnetusers_id = table.Column<string>(maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_request_proposal", x => x.id);
                    table.ForeignKey(
                        name: "fk_request_proposal_aspnetusers1",
                        column: x => x.aspnetusers_id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "response_proposal",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    type_of_proposal = table.Column<string>(maxLength: 150, nullable: true),
                    type_of_properties = table.Column<string>(maxLength: 8000, nullable: true),
                    price_minimum = table.Column<decimal>(type: "numeric", nullable: true),
                    price_maximum = table.Column<decimal>(type: "numeric", nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    aspnetusers_id = table.Column<string>(maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_response_proposal", x => x.id);
                    table.ForeignKey(
                        name: "fk_response_proposal_aspnetusers1",
                        column: x => x.aspnetusers_id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "province",
                columns: table => new
                {
                    id = table.Column<string>(maxLength: 150, nullable: false),
                    name = table.Column<string>(maxLength: 150, nullable: true),
                    name_ascii = table.Column<string>(maxLength: 150, nullable: true),
                    country_id = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_province", x => x.id);
                    table.ForeignKey(
                        name: "fk_province_country1",
                        column: x => x.country_id,
                        principalTable: "country",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bookmark_proposal",
                columns: table => new
                {
                    request_proposal_id = table.Column<long>(nullable: false),
                    aspnetusers_id = table.Column<string>(maxLength: 450, nullable: false),
                    created_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("bookmark_proposal_pkey", x => new { x.request_proposal_id, x.aspnetusers_id });
                    table.ForeignKey(
                        name: "fk_bookmark_proposal_aspnetusers1",
                        column: x => x.aspnetusers_id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_bookmark_proposal_request_proposal1",
                        column: x => x.request_proposal_id,
                        principalTable: "request_proposal",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "city",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    alternate_names = table.Column<string>(nullable: true),
                    f_code = table.Column<string>(maxLength: 150, nullable: true),
                    latitude = table.Column<string>(maxLength: 150, nullable: true),
                    longitude = table.Column<string>(maxLength: 150, nullable: true),
                    name = table.Column<string>(maxLength: 150, nullable: true),
                    name_ascii = table.Column<string>(maxLength: 150, nullable: true),
                    timezone = table.Column<string>(maxLength: 150, nullable: true),
                    province_id = table.Column<string>(maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_city", x => x.id);
                    table.ForeignKey(
                        name: "fk_cities_province1",
                        column: x => x.province_id,
                        principalTable: "province",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "location",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    postal_code = table.Column<string>(maxLength: 45, nullable: true),
                    street = table.Column<string>(maxLength: 150, nullable: true),
                    country_id = table.Column<string>(maxLength: 150, nullable: false),
                    province_id = table.Column<string>(maxLength: 150, nullable: false),
                    city_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_location", x => x.id);
                    table.ForeignKey(
                        name: "fk_location_city1",
                        column: x => x.city_id,
                        principalTable: "city",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_location_country1",
                        column: x => x.country_id,
                        principalTable: "country",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_location_province1",
                        column: x => x.province_id,
                        principalTable: "province",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "location_has_proposal",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    location_id = table.Column<long>(nullable: false),
                    request_proposal_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_location_has_proposal", x => x.id);
                    table.ForeignKey(
                        name: "fk_location_has_proposal_location1",
                        column: x => x.location_id,
                        principalTable: "location",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_location_has_proposal_request_proposal1",
                        column: x => x.request_proposal_id,
                        principalTable: "request_proposal",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "location_has_response",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    location_id = table.Column<long>(nullable: false),
                    response_proposal_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_location_has_response", x => x.id);
                    table.ForeignKey(
                        name: "fk_location_has_response_proposal_location1",
                        column: x => x.location_id,
                        principalTable: "location",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_location_has_response_response_proposal1",
                        column: x => x.response_proposal_id,
                        principalTable: "response_proposal",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "property",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    type = table.Column<string>(maxLength: 150, nullable: true),
                    sale_type = table.Column<string>(maxLength: 150, nullable: true),
                    description = table.Column<string>(nullable: true),
                    price = table.Column<decimal>(type: "numeric", nullable: true),
                    bedrooms = table.Column<string>(maxLength: 150, nullable: true),
                    bathrooms = table.Column<string>(maxLength: 150, nullable: true),
                    year_built = table.Column<long>(nullable: true),
                    characteristics = table.Column<string>(nullable: true),
                    status = table.Column<string>(maxLength: 150, nullable: true),
                    size = table.Column<string>(maxLength: 150, nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    location_id = table.Column<long>(nullable: false),
                    aspnetusers_id = table.Column<string>(maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_property", x => x.id);
                    table.ForeignKey(
                        name: "fk_property_aspnetusers1",
                        column: x => x.aspnetusers_id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_property_location1",
                        column: x => x.location_id,
                        principalTable: "location",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bookmark_property",
                columns: table => new
                {
                    property_id = table.Column<long>(nullable: false),
                    aspnetusers_id = table.Column<string>(maxLength: 450, nullable: false),
                    created_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("bookmark_property_pkey", x => new { x.property_id, x.aspnetusers_id });
                    table.ForeignKey(
                        name: "fk_bookmark_property_aspnetusers1",
                        column: x => x.aspnetusers_id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_bookmark_property_property1",
                        column: x => x.property_id,
                        principalTable: "property",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "property_photo",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    content_type = table.Column<string>(maxLength: 150, nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    name = table.Column<string>(maxLength: 150, nullable: true),
                    size = table.Column<decimal>(type: "numeric", nullable: true),
                    url = table.Column<string>(maxLength: 150, nullable: true),
                    property_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_property_photo", x => x.id);
                    table.ForeignKey(
                        name: "fk_property_photo_property1",
                        column: x => x.property_id,
                        principalTable: "property",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "request_has_response",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    property_id = table.Column<long>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    appointment_date = table.Column<DateTime>(nullable: true),
                    status = table.Column<string>(maxLength: 150, nullable: true),
                    request_proposal_id = table.Column<long>(nullable: true),
                    appointment_id = table.Column<long>(nullable: true),
                    response_proposal_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_request_has_response", x => x.id);
                    table.ForeignKey(
                        name: "fk_request_has_response_appointment1",
                        column: x => x.appointment_id,
                        principalTable: "appointment",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_proposal_has_proposal_property1",
                        column: x => x.property_id,
                        principalTable: "property",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_request_has_response_request_proposal1",
                        column: x => x.request_proposal_id,
                        principalTable: "request_proposal",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_request_has_response_response_proposal1",
                        column: x => x.response_proposal_id,
                        principalTable: "response_proposal",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "request_has_response_satus_log",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    request_has_response_satus_log_id = table.Column<long>(nullable: false),
                    status = table.Column<string>(maxLength: 150, nullable: true),
                    verb = table.Column<string>(maxLength: 150, nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    request_has_response_id = table.Column<long>(nullable: false),
                    aspnetusers_id = table.Column<string>(maxLength: 450, nullable: false),
                    aspnetusers_id_recipient = table.Column<string>(maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_request_has_response_satus_log", x => x.id);
                    table.ForeignKey(
                        name: "fk_request_has_response_satus_log_aspnetusers1",
                        column: x => x.aspnetusers_id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_request_has_response_satus_log_aspnetusers2",
                        column: x => x.aspnetusers_id_recipient,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_request_has_response_satus_log_request_has_response1",
                        column: x => x.request_has_response_id,
                        principalTable: "request_has_response",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_request_has_response_satus_log_request_has_response_satus_1",
                        column: x => x.request_has_response_satus_log_id,
                        principalTable: "request_has_response_satus_log",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "fk_appointment_aspnetusers1_idx",
                table: "appointment",
                column: "aspnetusers_id");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "fk_blog_aspnetusers1_idx",
                table: "blog",
                column: "aspnetusers_id");

            migrationBuilder.CreateIndex(
                name: "fk_bookmark_property_aspnetusers1_idx",
                table: "bookmark_property",
                column: "aspnetusers_id");

            migrationBuilder.CreateIndex(
                name: "fk_bookmark_property_property1_idx",
                table: "bookmark_property",
                column: "property_id");

            migrationBuilder.CreateIndex(
                name: "fk_bookmark_proposal_aspnetusers1_idx",
                table: "bookmark_proposal",
                column: "aspnetusers_id");

            migrationBuilder.CreateIndex(
                name: "fk_bookmark_proposal_request_proposal1_idx",
                table: "bookmark_proposal",
                column: "request_proposal_id");

            migrationBuilder.CreateIndex(
                name: "fk_cities_province1_idx",
                table: "city",
                column: "province_id");

            migrationBuilder.CreateIndex(
                name: "fk_location_city1_idx",
                table: "location",
                column: "city_id");

            migrationBuilder.CreateIndex(
                name: "fk_location_country1_idx",
                table: "location",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "fk_location_province1_idx",
                table: "location",
                column: "province_id");

            migrationBuilder.CreateIndex(
                name: "fk_location_has_proposal_location1_idx",
                table: "location_has_proposal",
                column: "location_id");

            migrationBuilder.CreateIndex(
                name: "fk_location_has_proposal_request_proposal1_idx",
                table: "location_has_proposal",
                column: "request_proposal_id");

            migrationBuilder.CreateIndex(
                name: "fk_location_has_response_proposal_location1_idx",
                table: "location_has_response",
                column: "location_id");

            migrationBuilder.CreateIndex(
                name: "fk_location_has_response_response_proposal1_idx",
                table: "location_has_response",
                column: "response_proposal_id");

            migrationBuilder.CreateIndex(
                name: "fk_property_aspnetusers1_idx",
                table: "property",
                column: "aspnetusers_id");

            migrationBuilder.CreateIndex(
                name: "fk_property_location1_idx",
                table: "property",
                column: "location_id");

            migrationBuilder.CreateIndex(
                name: "fk_property_photo_property1_idx",
                table: "property_photo",
                column: "property_id");

            migrationBuilder.CreateIndex(
                name: "fk_province_country1_id",
                table: "province",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "fk_request_has_response_appointment1_idx",
                table: "request_has_response",
                column: "appointment_id");

            migrationBuilder.CreateIndex(
                name: "fk_proposal_has_proposal_property1_idx",
                table: "request_has_response",
                column: "property_id");

            migrationBuilder.CreateIndex(
                name: "fk_request_has_response_request_proposal1_idx",
                table: "request_has_response",
                column: "request_proposal_id");

            migrationBuilder.CreateIndex(
                name: "fk_request_has_response_response_proposal1_idx",
                table: "request_has_response",
                column: "response_proposal_id");

            migrationBuilder.CreateIndex(
                name: "fk_request_has_response_satus_log_aspnetusers1_idx",
                table: "request_has_response_satus_log",
                column: "aspnetusers_id");

            migrationBuilder.CreateIndex(
                name: "fk_request_has_response_satus_log_aspnetusers2_idx",
                table: "request_has_response_satus_log",
                column: "aspnetusers_id_recipient");

            migrationBuilder.CreateIndex(
                name: "fk_request_has_response_satus_log_request_has_response1_idx",
                table: "request_has_response_satus_log",
                column: "request_has_response_id");

            migrationBuilder.CreateIndex(
                name: "fk_request_has_response_satus_log_request_has_response_satu_idx",
                table: "request_has_response_satus_log",
                column: "request_has_response_satus_log_id");

            migrationBuilder.CreateIndex(
                name: "fk_request_proposal_aspnetusers1_idx",
                table: "request_proposal",
                column: "aspnetusers_id");

            migrationBuilder.CreateIndex(
                name: "fk_response_proposal_aspnetusers1_idx",
                table: "response_proposal",
                column: "aspnetusers_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "auto_increment");

            migrationBuilder.DropTable(
                name: "blog");

            migrationBuilder.DropTable(
                name: "bookmark_property");

            migrationBuilder.DropTable(
                name: "bookmark_proposal");

            migrationBuilder.DropTable(
                name: "location_has_proposal");

            migrationBuilder.DropTable(
                name: "location_has_response");

            migrationBuilder.DropTable(
                name: "property_photo");

            migrationBuilder.DropTable(
                name: "request_has_response_satus_log");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "request_has_response");

            migrationBuilder.DropTable(
                name: "appointment");

            migrationBuilder.DropTable(
                name: "property");

            migrationBuilder.DropTable(
                name: "request_proposal");

            migrationBuilder.DropTable(
                name: "response_proposal");

            migrationBuilder.DropTable(
                name: "location");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "city");

            migrationBuilder.DropTable(
                name: "province");

            migrationBuilder.DropTable(
                name: "country");
        }
    }
}
