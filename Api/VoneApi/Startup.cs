using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HotChocolate;
using HotChocolate.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using VoneApi.Data;
using VoneApi.Helpers;
using VoneApi.Models;
using VoneApi.Services;
using VoneApi.Type;

namespace VoneApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));

            //Add distributed cache service backed by Redis cache
            services.AddStackExchangeRedisCache(option =>
            {
                option.InstanceName = Configuration.GetConnectionString("redis:name");
                option.Configuration = Configuration.GetConnectionString("redis:host");
            });

            // Register the graphQl service
            services.AddGraphQL(s => SchemaBuilder.New()
                .AddServices(s)
                .AddType<BlogType>()
                .AddQueryType<Query>()
                .Create());

            // services.AddErrorFilter<BookNotFoundExceptionFilter>();



            services.AddAutoMapper(typeof(AutoMapperProfile).Assembly);

            services.AddTransient<UtilService, UtilService>();
            // services.AddSingleton<IAuthorService, InMemoryAuthorService>();
            services.AddTransient<IBlogInterface, BlogService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UsePlayground();
            }

            app.UseHttpsRedirection();

            app.UseGraphQL("/graphql");

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
