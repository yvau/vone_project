using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VoneApi.BindingModels;
using VoneApi.Data;
using VoneApi.Models;
using VoneApi.Services;
using VoneApi.Validation;

namespace Proov.Controllers
{
    [Route("api/")]
    [ApiController]
    public class RequestProposalsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private UtilService _utilService;

        public RequestProposalsController(ApplicationDbContext context, UtilService utilService)
        {
            _context = context;
            _utilService = utilService;
        }


        // GET: api/Proposals1
        /*[HttpGet]
        public async Task<ActionResult<IEnumerable<RequestProposal>>> GetProposal()
        {
            return await _context.RequestProposal.ToListAsync();
        }*/

        // POST: api/Proposals1
        [HttpPost("rent/new/")]
        public async Task<ActionResult<RequestProposalModelView>> NewRentingProposal(RequestProposalModelView model)
        {
            // check validation
            RentingProposalValidator validator = new RentingProposalValidator();
            ValidationResult results = validator.Validate(model);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilService.DisplayErrors(results.Errors);
                return Ok(errors);
            }

            var buy = new RequestProposal
            {
                Id = model.Id,
                Bathrooms = model.Bathrooms,
                Bedrooms = model.Bedrooms,
                Size = model.Size,
                NumberOfParking = model.NumberOfParking,
                NumberOfGarage = model.NumberOfGarage,
                TypeOfProposal = model.TypeOfProposal,
                PriceMinimum = model.PriceMinimum,
                PriceMaximum = model.PriceMaximum,
                IsCurrentlyOwner = model.IsCurrentlyOwner,
                IsThereContingency = model.IsThereContingency,
                IsWithPool = model.IsWithPool,
                IsPreApproved = model.IsPreApproved,
                BankingInstitution = model.BankingInstitution,
                Urgency = model.Urgency,
                IsEnabled = model.IsEnabled,
                IsPublished = model.IsPublished,
                IsFirstBuyer = model.IsFirstBuyer,
                IsFurnished = model.IsFurnished,
                Elevator = model.Elevator,
                Wheelchair = model.Wheelchair,
                NearbySchool = model.NearbySchool,
                NearbyParc = model.NearbyParc,
                NearbySportsCenter = model.NearbySportsCenter,
                NearbyTrade = model.NearbyTrade,
                NearbyPublicTransport = model.NearbyPublicTransport,
                NearbyWaterfront = model.NearbyWaterfront,
                NearbyNavigableWaterBody = model.NearbyNavigableWaterBody,
                TypeOfProperties = model.TypeOfProperties,
                Status = model.Status,
                AgeOfProperty = model.AgeOfProperty,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt
                // AspNetUsersId
                // ProfileId = 1
            };

            _context.RequestProposal.Add(buy);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProposalExists(buy.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetProposal", new { id = buy.Id }, buy);
        }

        // POST: api/Proposals1
        [HttpPost("buy/new/")]
        public async Task<ActionResult<RequestProposalModelView>> NewBuyingProposal(RequestProposalModelView model)
        {
            // check validation
            BuyingProposalValidator validator = new BuyingProposalValidator();
            ValidationResult results = validator.Validate(model);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilService.DisplayErrors(results.Errors);
                return Ok(errors);
            }

            var buy = new RequestProposal
            {
                Id = model.Id,
                Bathrooms = model.Bathrooms,
                Bedrooms = model.Bedrooms,
                Size = model.Size,
                NumberOfParking = model.NumberOfParking,
                NumberOfGarage = model.NumberOfGarage,
                TypeOfProposal = model.TypeOfProposal,
                PriceMinimum = model.PriceMinimum,
                PriceMaximum = model.PriceMaximum,
                IsCurrentlyOwner = model.IsCurrentlyOwner,
                IsThereContingency = model.IsThereContingency,
                IsWithPool = model.IsWithPool,
                IsPreApproved = model.IsPreApproved,
                BankingInstitution = model.BankingInstitution,
                Urgency = model.Urgency,
                IsEnabled = model.IsEnabled,
                IsPublished = model.IsPublished,
                IsFirstBuyer = model.IsFirstBuyer,
                IsFurnished = model.IsFurnished,
                Elevator = model.Elevator,
                Wheelchair = model.Wheelchair,
                NearbySchool = model.NearbySchool,
                NearbyParc = model.NearbyParc,
                NearbySportsCenter = model.NearbySportsCenter,
                NearbyTrade = model.NearbyTrade,
                NearbyPublicTransport = model.NearbyPublicTransport,
                NearbyWaterfront = model.NearbyWaterfront,
                NearbyNavigableWaterBody = model.NearbyNavigableWaterBody,
                TypeOfProperties = model.TypeOfProperties,
                Status = model.Status,
                AgeOfProperty = model.AgeOfProperty,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt
                // AspNetUsersId
                // ProfileId = 1
            };

            _context.RequestProposal.Add(buy);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProposalExists(buy.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetProposal", new { id = buy.Id }, buy);
        }

        // GET: api/Proposals1/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RequestProposal>> GetProposal(long id)
        {
            var proposal = await _context.RequestProposal.FindAsync(id);

            if (proposal == null)
            {
                return NotFound();
            }

            return proposal;
        }

        // PUT: api/Proposals1/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProposal(long id, RequestProposal proposal)
        {
            if (id != proposal.Id)
            {
                return BadRequest();
            }

            _context.Entry(proposal).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProposalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Proposals1
        [HttpPost]
        public async Task<ActionResult<RequestProposal>> PostProposal(RequestProposal proposal)
        {
            _context.RequestProposal.Add(proposal);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProposalExists(proposal.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetProposal", new { id = proposal.Id }, proposal);
        }

        // DELETE: api/Proposals1/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RequestProposal>> DeleteProposal(long id)
        {
            var proposal = await _context.RequestProposal.FindAsync(id);
            if (proposal == null)
            {
                return NotFound();
            }

            _context.RequestProposal.Remove(proposal);
            await _context.SaveChangesAsync();

            return proposal;
        }

        private bool ProposalExists(long id)
        {
            return _context.RequestProposal.Any(e => e.Id == id);
        }
    }
}
