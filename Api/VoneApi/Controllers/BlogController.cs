﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.Results;
using GraphQL;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VoneApi.BindingModels;
using VoneApi.Data;
using VoneApi.Helpers;
using VoneApi.Models;
using VoneApi.Services;
using VoneApi.Type;
using VoneApi.Validation;
using VoneApi.Validators;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VoneApi.Controllers
{
    [Route("api/")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private UtilService _utilService;

        public BlogController(ApplicationDbContext context, IMapper mapper, UtilService utilService)
        {
            _context = context;
            _mapper = mapper;
            _utilService = utilService;
        }


        // GET: Students
        [HttpGet("blog/")]
        public OkObjectResult GetAll(string sortOrder, string currentFilter, string searchString, int? pageNumber, int? pageSize)
        {

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var blogs = from s in _context.Blog
                        select new { Id = s.Id,
                                     Title = s.Title,
                                     TitleSlug = s.TitleSlug,
                                     Body = s.Body,
                                     Tags = s.Tags,
                                     CreatedAt = s.CreatedAt};
            if (!String.IsNullOrEmpty(searchString))
            {
                blogs = blogs.Where(s => s.Title.Contains(searchString)
                                       || s.TitleSlug.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    blogs = blogs.OrderByDescending(s => s.Title);
                    break;
                case "Date":
                    blogs = blogs.OrderBy(s => s.CreatedAt);
                    break;
                case "date_desc":
                    blogs = blogs.OrderByDescending(s => s.CreatedAt);
                    break;
                default:
                    blogs = blogs.OrderBy(s => s.Title);
                    break;
            }


            var paginationResponse = PaginationHelpers.CreatePaginatedResponse(blogs, pageNumber ?? 1, pageSize ?? 9, _utilService);
            return Ok(paginationResponse);
        }

        /*[HttpGet("blog/")]
        public async Task<IActionResult> GetAll([FromQuery] PaginationQuery _paginationQuery)
        {
            var _pagination = _mapper.Map<PaginationFilter>(_paginationQuery);
            var _blogs = await GetPostsAsync(_pagination);
            var _blogModelResponse = _mapper.Map<List<BlogModelView>>(_blogs);

            if (_paginationQuery == null || _paginationQuery.PageNumber < 1 || _paginationQuery.PageSize < 1)
            {
                return Ok(new PagedResponse<PostResponse>(postsResponse));
            }

            var paginationResponse = PaginationHelpers.CreatePaginatedResponse(_utilService, _pagination, _blogModelResponse);
            return Ok(paginationResponse);
        }*/

        // GET: api/Blogs/5
        [HttpGet("blog/{id}/")]
        public async Task<ActionResult<Blog>> GetBlog(long id)
        {
            var blog = await _context.Blog.FindAsync(id);

            if (blog == null)
            {
                return NotFound();
            }

            return blog;
        }

        // PUT: api/Blogs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("blog/{id}/edit")]
        public async Task<IActionResult> PutBlog(long id, BlogModelView model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }

            // check validation
            BlogValidator validator = new BlogValidator();
            ValidationResult results = validator.Validate(model);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilService.DisplayErrors(results.Errors);
                return Ok(errors);
            }

            var blog = new Blog
            {
                Id = model.Id,
                Title = model.Title,
                TitleSlug = model.TitleSlug,
                Body = model.Body,
                IsPublished = model.IsPublished,
                ImagePreviewUrl = model.ImagePreviewUrl,
                CreatedAt = model.CreatedAt,
                Tags = model.Category,
                Enabled = true,
                // ProfileId = 1
            };

            _context.Entry(blog).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BlogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            // return NoContent();
            return Ok(new ApiResponse(true, "your blog has been updated"));
        }

        // POST: api/Blogs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost("blog/new/")]
        public async Task<ActionResult<BlogModelView>> PostBlog(BlogModelView blogModelView)
        {
            // check validation
            BlogValidator validator = new BlogValidator();
            ValidationResult results = validator.Validate(blogModelView);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilService.DisplayErrors(results.Errors);
                return Ok(errors);
            }

            var blog = new Blog
            {
                Id = _utilService.GetId(Resources.BLOG),
                Title = blogModelView.Title,
                TitleSlug = blogModelView.TitleSlug,
                Body = blogModelView.Body,
                IsPublished = blogModelView.IsPublished,
                ImagePreviewUrl = blogModelView.ImagePreviewUrl,
                CreatedAt = blogModelView.CreatedAt,
                Tags = blogModelView.Category,
                Enabled = true,
                // ProfileId = 1
            };

            _context.Blog.Add(blog);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BlogExists(blog.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(new ApiResponse(true, "your blog has been saved"));
        }

        // DELETE: api/Blogs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Blog>> DeleteBlog(long id)
        {
            var blog = await _context.Blog.FindAsync(id);
            if (blog == null)
            {
                return NotFound();
            }

            _context.Blog.Remove(blog);
            await _context.SaveChangesAsync();

            return blog;
        }

        private bool BlogExists(long id)
        {
            return _context.Blog.Any(e => e.Id == id);
        }

        private async Task<List<Blog>> GetPostsAsync(PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _context.Blog.Include(x => x.Tags).ToListAsync();
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;
            return await _context.Blog.Include(x => x.Aspnetusers)
                .Skip(skip).Take(paginationFilter.PageSize).ToListAsync();
            // return await _context.Blog.Skip(skip).Take(paginationFilter.PageSize).ToListAsync();
        }
    }
}
