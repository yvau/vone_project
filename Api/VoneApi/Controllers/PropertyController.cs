﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VoneApi.BindingModels;
using VoneApi.Data;
using VoneApi.Models;
using VoneApi.Services;
using VoneApi.Validators;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VoneApi.Controllers
{
    [Route("api/property/")]
    [ApiController]
    public class PropertyController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly UtilService _utilService;

        public PropertyController(ApplicationDbContext context,
                                  UtilService utilService,
                                  IMapper mapper)
        {
            _mapper = mapper;
            _utilService = utilService;
            _context = context;
        }

        // GET: api/Properties
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Property>>> GetProperty()
        {
            return await _context.Property.ToListAsync();
        }

        // GET: api/Properties/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Property>> GetProperty(long id)
        {
            var @property = await _context.Property.FindAsync(id);

            if (@property == null)
            {
                return NotFound();
            }

            return @property;
        }

        // PUT: api/Properties/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProperty(long id, Property @property)
        {
            if (id != @property.Id)
            {
                return BadRequest();
            }

            _context.Entry(@property).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PropertyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Properties
        [HttpPost("new/")]
        // public async Task<ActionResult<Property>> PostProperty(PropertyModel model)
        public async Task<IActionResult> PostProperty(PropertyModel model)
        {
            // check validation
            PropertyValidator validator = new PropertyValidator();
            ValidationResult results = validator.Validate(model);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilService.DisplayErrors(results.Errors);
                return Ok(errors);
                // return Content(HttpStatusCode.BadRequest,  new HttpError(ModelState, includeErrorDetail: true)
            }
            var @property = new Property();

            /*_context.Property.Add(@property);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PropertyExists(@property.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }*/

            return Ok();
            // return CreatedAtAction("GetProperty", new { id = @property.Id }, @property);
        }

        // DELETE: api/Properties/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Property>> DeleteProperty(long id)
        {
            var @property = await _context.Property.FindAsync(id);
            if (@property == null)
            {
                return NotFound();
            }

            _context.Property.Remove(@property);
            await _context.SaveChangesAsync();

            return @property;
        }

        private bool PropertyExists(long id)
        {
            return _context.Property.Any(e => e.Id == id);
        }
    }
}
