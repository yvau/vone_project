﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VoneApi.BindingModels;
using VoneApi.Data;
using VoneApi.Helpers;
using VoneApi.Models;
using VoneApi.Services;
using VoneApi.Validation;
using VoneApi.Validators;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VoneApi.Controllers
{
    [Route("api/")]
    [ApiController]
    public class ResponseProposalsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private UtilService _utilService;

        public ResponseProposalsController(ApplicationDbContext context, UtilService utilService)
        {
            _context = context;
            _utilService = utilService;
        }

        // POST: api/Sell/new
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost("sell/new/")]
        public async Task<ActionResult<ResponseProposalModelView>> PostSell(ResponseProposalModelView model)
        {
            // check validation
            SellingProposalValidator validator = new SellingProposalValidator();
            ValidationResult results = validator.Validate(model);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilService.DisplayErrors(results.Errors);
                return Ok(errors);
            }

            /*var blog = new Blog
            {
                Id = _utilService.GetId(Resources.BLOG),
                Title = blogModelView.Title,
                TitleSlug = blogModelView.TitleSlug,
                Body = blogModelView.Body,
                IsPublished = blogModelView.IsPublished,
                ImagePreviewUrl = blogModelView.ImagePreviewUrl,
                CreatedAt = blogModelView.CreatedAt,
                Tags = blogModelView.Category,
                Enabled = true,
                // ProfileId = 1
            };

            _context.Blog.Add(blog);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BlogExists(blog.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }*/

            return Ok(new ApiResponse(true, "your blog has been saved"));
        }

        // POST: api/Sell/new
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost("lease/new/")]
        public async Task<ActionResult<ResponseProposalModelView>> PostLease(ResponseProposalModelView model)
        {
            // check validation
            LeasingProposalValidator validator = new LeasingProposalValidator();
            ValidationResult results = validator.Validate(model);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilService.DisplayErrors(results.Errors);
                return Ok(errors);
            }

            /*var blog = new Blog
            {
                Id = _utilService.GetId(Resources.BLOG),
                Title = blogModelView.Title,
                TitleSlug = blogModelView.TitleSlug,
                Body = blogModelView.Body,
                IsPublished = blogModelView.IsPublished,
                ImagePreviewUrl = blogModelView.ImagePreviewUrl,
                CreatedAt = blogModelView.CreatedAt,
                Tags = blogModelView.Category,
                Enabled = true,
                // ProfileId = 1
            };

            _context.Blog.Add(blog);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BlogExists(blog.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }*/

            return Ok(new ApiResponse(true, "your blog has been saved"));
        }
    }
}
