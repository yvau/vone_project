﻿using Bogus;
using Hangfire;
using BackgroundJobs.Data;
using BackgroundJobs.Helpers;
using BackgroundJobs.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BackgroundJobs.Services
{
    public class BookmarkPropertyJobService : IJob
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _applicationDbContext;
        private static Random random = new Random();

        public BookmarkPropertyJobService(UserManager<ApplicationUser> userManager,
                              ApplicationDbContext applicationDbContext)
        {
            _userManager = userManager;
            _applicationDbContext = applicationDbContext;
        }

        public async Task Run(IJobCancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            await Create();
        }
        public async Task Create()
        {
            var roleList = new[] { "SELLER", "LESSOR" };

            try
            {
                for (int x = 0; x < random.Next(100); x++)
                {
                    var _faker = new Faker();

                    var _usersInRole = await _userManager.GetUsersInRoleAsync(_faker.PickRandom(roleList));
                    var _properties = new List<Property>();

                    if (_faker.PickRandom(roleList).Equals("SELLER"))
                    {
                        _properties = _applicationDbContext.Property.Where(r => r.Type.Equals("FOR_SALE")).ToList();
                    }
                    else
                    {
                        _properties = _applicationDbContext.Property.Where(r => r.Type.Equals("FOR_RENT")).ToList();
                    }

                    int indexProfile = random.Next(_usersInRole.Count);
                    int indexProperty = random.Next(_properties.Count);

                    BookmarkProperty _bookmarkProperty = new BookmarkProperty();
                    _bookmarkProperty.PropertyId = _properties[indexProperty].Id;
                    _bookmarkProperty.AspnetusersId = _usersInRole[indexProfile].Id;
                    _bookmarkProperty.CreatedAt = DateTime.Now;


                    _applicationDbContext.Add(_bookmarkProperty);
                    await _applicationDbContext.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"The exception {e}");
            }
        }
    }
}
