﻿using BackgroundJobs.Data;
using BackgroundJobs.Helpers;
using BackgroundJobs.Models;
using Bogus;
using Hangfire;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BackgroundJobs.Services
{
    public class BuyJobService : IJob
    {
        private readonly IAutoIncrementInterface _autoIncrementInterface;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _applicationDbContext;
        private static Random random = new Random();

        public BuyJobService(IAutoIncrementInterface autoIncrementInterface, 
                              UserManager<ApplicationUser> userManager,
                              ApplicationDbContext applicationDbContext) 
        {
            _userManager = userManager;
            _autoIncrementInterface = autoIncrementInterface;
            _applicationDbContext = applicationDbContext;
        }

        public async Task Run(IJobCancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            await Create();
        }
        public async Task Create()
        {
            try
            {
                var randomValue = random.Next(50, 100);

                for (int i = 0; i < randomValue; i++)
                {
                    var _cities = _applicationDbContext.City.Include(p => p.Province).ThenInclude(c => c.Country).Where(r => r.ProvinceId.Equals("CA.10")).ToList();
                    var _cityCount = _cities.Count();
                    int _locationToTake = random.Next(1, 7);
                    var _location = _cities.Skip(random.Next(0, _cityCount)).Take(_locationToTake).ToList();

                    var _usersInRole = await _userManager.GetUsersInRoleAsync("BUYER");
                    var _profiles = _usersInRole.Where(r => r.RequestProposal.Count(r => r.IsEnabled.Equals("ENABLED") && r.TypeOfProposal.Equals("FOR_SALE")) < 4).ToList();
                    int index = random.Next(_profiles.Count);

                    var typeOfPropertyList = new[] { "FLAT_APARTMENT",
                                   "MULTI_FAMILY_HOUSE",
                                   "SINGLE_FAMILY_HOUSE",
                                   "TOWNHOUSE",
                                   "CONDOMINIUM",
                                   "SEMI_DETACHED_HOUSE",
                                   "SEMI_DETACHED_DUPLEX",
                                   "MOBILE_HOME",
                                   "CO_OP",
                                   "LAND",

                                   "FLAT_APARTMENT,MULTI_FAMILY_HOUSE",
                                   "FLAT_APARTMENT,MULTI_FAMILY_HOUSE,SINGLE_FAMILY_HOUSE",
                                   "TOWNHOUSE,CONDOMINIUM",
                                   "FLAT_APARTMENT,CONDOMINIUM",
                                   "SEMI_DETACHED_HOUSE,SEMI_DETACHED_DUPLEX",
                                   "MOBILE_HOME,CO_OP"};
                    var bankList = new[] { "TD Canada Trust",
                                       "Scotiabank",
                                       "BMO",
                                       "CIBC"
                    };

                    var _faker = new Faker();
                    RequestProposal _requestProposal = new RequestProposal();
                    _requestProposal.Id = _autoIncrementInterface.GetId(Resources.REQUESTPROPOSAL);
                    _requestProposal.TypeOfProperties = _faker.PickRandom(typeOfPropertyList);

                    if (!_requestProposal.TypeOfProperties.Contains("LAND"))
                    {
                        _requestProposal.Bathrooms = _faker.Random.Int(1, 6);
                        _requestProposal.Bedrooms = _faker.Random.Int(1, 6);
                        _requestProposal.NumberOfParking = _faker.Random.Int(1, 3);
                        _requestProposal.NumberOfGarage = _faker.Random.Int(1, 3);
                        _requestProposal.IsWithPool = _faker.PickRandom(new[] { "IS_WITH_POOL",
                                                                                    "IS_NOT_WITH_POOL",
                                                                                    "IS_ANY_WITH_POOL"});
                        _requestProposal.Size = _faker.Random.Decimal(1, 6000);
                        _requestProposal.Elevator = _faker.Random.Bool().ToString();
                        _requestProposal.Wheelchair = _faker.Random.Bool().ToString();
                        _requestProposal.NearbySchool = _faker.PickRandom(new[] { "IS_NEARBY_SCHOOL",
                                                                                    "IS_NOT_NEARBY_SCHOOL",
                                                                                    "IS_ANY_NEARBY_SCHOOL"});
                        _requestProposal.NearbyParc = _faker.PickRandom(new[] { "IS_NEARBY_PARC",
                                                                                    "IS_NOT_NEARBY_PARC",
                                                                                    "IS_ANY_NEARBY_PARC"});
                        _requestProposal.NearbySportsCenter = _faker.PickRandom(new[] { "IS_NEARBY_SPORT_CENTER",
                                                                                    "IS_NOT_NEARBY_SPORT_CENTER",
                                                                                    "IS_ANY_NEARBY_SPORT_CENTER"});
                        _requestProposal.NearbyTrade = _faker.PickRandom(new[] { "IS_NEARBY_TRADE",
                                                                                    "IS_NOT_NEARBY_TRADE",
                                                                                    "IS_ANY_NEARBY_TRADE"});
                        _requestProposal.NearbyPublicTransport = _faker.PickRandom(new[] { "IS_NEARBY_PUBLIC_TRANSPORT",
                                                                                    "IS_NOT_NEARBY_PUBLIC_TRANSPORT",
                                                                                    "IS_ANY_NEARBY_PUBLIC_TRANSPORT"});
                        _requestProposal.NearbyWaterfront = _faker.PickRandom(new[] { "IS_NEARBY_WATER_FRONT",
                                                                                    "IS_NOT_NEARBY_WATER_FRONT",
                                                                                    "IS_ANY_NEARBY_WATER_FRONT"});
                        _requestProposal.NearbyNavigableWaterBody = _faker.PickRandom(new[] { "IS_NEARBY_NAVIGABLE_WATER_BODY",
                                                                                    "IS_NOT_NEARBY_NAVIGABLE_WATER_BODY",
                                                                                    "IS_ANY_NEARBY_NAVIGABLE_WATER_BODY"});
                    }
                    else
                    {
                        _requestProposal.Size = _faker.Random.Decimal(3000, 300000);
                    }
                    _requestProposal.TypeOfProposal = "FOR_SALE";
                    _requestProposal.PriceMinimum = _faker.Random.Decimal(0, 1500000);
                    _requestProposal.PriceMaximum = _faker.Random.Decimal(1500000, 1500000000);
                    _requestProposal.IsCurrentlyOwner = _faker.Random.Bool();
                    _requestProposal.IsThereContingency = _faker.Random.Bool();
                    _requestProposal.IsPreApproved = _faker.Random.Bool().ToString();
                    if (_requestProposal.IsPreApproved == "true")
                    {
                        _requestProposal.BankingInstitution = _faker.PickRandom(bankList);
                    }
                    _requestProposal.Urgency = _faker.PickRandom(new[] { "IS_URGENT",
                                                                             "IS_NOT_URGENT",
                                                                             "IS_ANY_URGENT"});
                    _requestProposal.IsEnabled = "ENABLED";
                    _requestProposal.IsPublished = "IS_PUBLISHED";
                    _requestProposal.IsFirstBuyer = _faker.Random.Bool().ToString();

                    _requestProposal.Status = "ENABLED";
                    _requestProposal.AgeOfProperty = _faker.PickRandom(new[] { "IS_AGE_NEW",
                                                                                   "IS_AGE_NOT_NEW",
                                                                                   "IS_ANY_AGE"});
                    _requestProposal.CreatedAt = DateTime.Now;
                    _requestProposal.UpdatedAt = DateTime.Now;
                    _requestProposal.AspnetusersId = _profiles[index].Id;
                    _requestProposal.Aspnetusers = _profiles[index];

                    // We init the location has proposal
                    for (int j = 0; j < _location.Count(); j++)
                    {
                        var _fakerLocationHasProposals = new Faker(); // default en
                        var locationHasProposalId = _autoIncrementInterface.GetId(Resources.LOCATIONHASPROPOSAL);
                        var location = _autoIncrementInterface.GetId(Resources.LOCATION);

                        var _locationHasProposal = new LocationHasProposal
                        {
                            Id = locationHasProposalId,
                            Location = new Location
                            {
                                Id = location,
                                PostalCode = _fakerLocationHasProposals.Address.ZipCode(_location[j].Province.CountryId),
                                Street = _fakerLocationHasProposals.Address.StreetAddress(),
                                Country = _location[j].Province.Country,
                                Province = _location[j].Province,
                                City = _location[j]
                            },
                            RequestProposal = _requestProposal
                        };
                        _applicationDbContext.Add(_locationHasProposal);
                        _applicationDbContext.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"The exception {e}");
            }
        }
    }
}
