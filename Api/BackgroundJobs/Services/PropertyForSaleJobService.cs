﻿using Bogus;
using Hangfire;
using BackgroundJobs.Data;
using BackgroundJobs.Helpers;
using BackgroundJobs.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BackgroundJobs.Services
{
    public class PropertyForSaleJobService : IJob
    {
        private readonly IAutoIncrementInterface _autoIncrementInterface;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _applicationDbContext;
        private static Random random = new Random();

        public PropertyForSaleJobService(IAutoIncrementInterface autoIncrementInterface, 
                              UserManager<ApplicationUser> userManager,
                              ApplicationDbContext applicationDbContext) 
        {
            _userManager = userManager;
            _autoIncrementInterface = autoIncrementInterface;
            _applicationDbContext = applicationDbContext;
        }

        public async Task Run(IJobCancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            await Create();
        }
        public async Task Create()
        {
            try
            {
                for (int i = 0; i < random.Next(50, 100); i++)
                {
                    var typeOfPropertyList = new[] { "FLAT_APARTMENT",
                                   "MULTI_FAMILY_HOUSE",
                                   "SINGLE_FAMILY_HOUSE",
                                   "TOWNHOUSE",
                                   "CONDOMINIUM",
                                   "SEMI_DETACHED_HOUSE",
                                   "SEMI_DETACHED_DUPLEX",
                                   "MOBILE_HOME",
                                   "CO_OP",
                                   "LAND"};

                    var photosPropertyType = new[] { "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927428/property/pexels-photo-574188.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927429/property/pexels-photo-5563472.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927421/property/pexels-photo-1115804.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927419/property/pexels-photo-2462015.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927416/property/pexels-photo-565324.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927416/property/pexels-photo-1182703.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927416/property/pexels-photo-2893177.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927412/property/pexels-photo-1022936.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927409/property/pexels-photo-1005477.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927409/property/pexels-photo-534228.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927407/property/pexels-photo-462205.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927406/property/pexels-photo-439391.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927404/property/pexels-photo-2079434.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927403/property/pexels-photo-2347721.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927400/property/pexels-photo-323781.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927400/property/pexels-photo-280221.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927400/property/pexels-photo-347141.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927396/property/pexels-photo-280229.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927394/property/pexels-photo-323780.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927392/property/pexels-photo-262405.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927390/property/pexels-photo-323776.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927390/property/pexels-photo-323772.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927386/property/pexels-photo-323705.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927385/property/pexels-photo-279607.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927382/property/pexels-photo-276554.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927380/property/pexels-photo-277667.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927379/property/pexels-photo-1974596.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927378/property/pexels-photo-276724.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927375/property/pexels-photo-259593.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927375/property/pexels-photo-259962.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927375/property/pexels-photo-2030037.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927374/property/pexels-photo-259751.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927371/property/pexels-photo-259646.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927370/property/pexels-photo-1438832.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927368/property/pexels-photo-1643389.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927367/property/pexels-photo-259685.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927364/property/pexels-photo-1546166.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927358/property/pexels-photo-1396132.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927363/property/pexels-photo-1643384.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927358/property/pexels-photo-1546168.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927357/property/pexels-photo-259580.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927354/property/pexels-photo-226407.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927352/property/pexels-photo-106399.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927349/property/pexels-photo-1396122.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927346/property/pexels-photo-221540.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927344/property/pexels-photo-210617.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927343/property/pexels-photo-126271.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927341/property/pexels-photo-209296.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927336/property/pexels-photo-208736.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927334/property/pexels-photo-157043.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927332/property/pexels-photo-164522.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927330/property/italy-mountains-dawn-daybreak-147411.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604927329/property/large-home-residential-house-architecture-53610.jpg"};

                    var photosPropertyLand = new[] {
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604928684/pexels-photo-5641347_yo9ro8.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604928685/pexels-photo-1237777_xmeqh5.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604928685/pexels-photo-1097016_tqkabo.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604928684/pexels-photo-440731_oy1iqx.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604928683/pexels-photo-1000057_mdg7dj.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604928683/pexels-photo-636342_abyhmd.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604928683/pexels-photo-102728_hstjbe.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604928683/field-clouds-sky-earth-46160_lfnaj8.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604928683/pexels-photo-388415_bu3mdi.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604928682/amazing-beautiful-beauty-blue_izmqs6.jpg",
                                         "https://res.cloudinary.com/dc5rinehl/image/upload/v1604928682/pexels-photo-247599_kwagcz.jpg"
                    };

                    var provinceList = new[] { "CA.10" };

                    var contentTypeList = new[] { "image/bmp",
                                   "image/gif",
                                   "image/jpeg",
                                   "image/png",
                                   "image/svg+xml",
                                   "image/tiff",
                                   "image/webp"};

                    var _usersInRole = await _userManager.GetUsersInRoleAsync("SELLER");
                    var _profiles = _usersInRole.Where(r => r.Property.Count(p => /*p.IsEnabled.Equals("ENABLED") && */p.Type.Equals("FOR_SALE")) < 20).ToList();
                    int index = random.Next(_profiles.Count);

                    var _province = _applicationDbContext.Province.Include(c => c.Country).Where(p => p.Id.Equals("CA.10")).FirstOrDefault();

                    var _cities = _applicationDbContext.City.Where(c => c.ProvinceId.Equals("CA.10")).ToList();
                    var _skipCity = random.Next(0, _cities.Count());
                    var _city = _cities.Skip(_skipCity).Take(1).FirstOrDefault();
                    

                    var _faker = new Faker();
                    var propertyType = _faker.PickRandom(typeOfPropertyList);

                    Property _property = new Property();
                    _property.Id = _autoIncrementInterface.GetId(Resources.REQUESTPROPOSAL);
                    _property.Type = propertyType;
                    _property.SaleType = "FOR_RENT";
                    _property.Description = _faker.Lorem.Paragraphs();
                    _property.Price = random.Next(200, 10000);
                    _property.YearBuilt = random.Next(1980, 2020);
                    _property.Status = "ENABLED";
                    _property.Size = random.Next(200, 10000).ToString();
                    _property.CreatedAt = DateTime.Now;
                    _property.UpdatedAt = DateTime.Now;
                    _property.Location = new Location
                    {
                        Id = _autoIncrementInterface.GetId(Resources.LOCATION),
                        CityId = _city.Id,
                        City = _city,
                        CountryId = _province.CountryId,
                        Country = _province.Country,
                        ProvinceId = _province.Id,
                        Province = _province,
                        PostalCode = new Randomizer().Replace("?#? #?#"),
                        Street = _faker.Address.StreetAddress()
                    };
                    _property.AspnetusersId = _profiles[index].Id;

                    // We init the location has proposal
                    List<PropertyPhoto> _propertyPhotos = new List<PropertyPhoto>();

                    for (int j = 0; j < random.Next(5); j++)
                    {
                        var _fakerPropertyPhoto = new Faker();
                        _propertyPhotos.Add(new PropertyPhoto
                        {
                            Id = _autoIncrementInterface.GetId(Resources.PROPERTYPHOTO),
                            CreatedAt = DateTime.Now,
                            ContentType = _fakerPropertyPhoto.PickRandom(contentTypeList),
                            Name = _fakerPropertyPhoto.Lorem.Word(),
                            Property = _property,
                            Size = _fakerPropertyPhoto.Random.Number(120),
                            Url = (propertyType.Equals("LAND")) ? _fakerPropertyPhoto.PickRandom(photosPropertyLand) : _fakerPropertyPhoto.PickRandom(photosPropertyType)
                        });
                    }

                    _applicationDbContext.AddRange(_propertyPhotos);
                    await _applicationDbContext.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"The exception {e}");
            }
        }
    }
}
