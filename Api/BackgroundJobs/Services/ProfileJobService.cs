﻿using Bogus;
using Hangfire;
using BackgroundJobs.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackgroundJobs.Services
{
    public class ProfileJobService : IJob
    {
        private static Random random = new Random();
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<ProfileJobService> _logger;

        public ProfileJobService(UserManager<ApplicationUser> userManager,
                                ILogger<ProfileJobService> logger)
        {
            _userManager = userManager;
            _logger = logger;
        }

        public async Task Run(IJobCancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            await Create();
        }

        public async Task Create()
        {
            var genderList = new[] { "M", "F" };
            var languageList = new[] { "en", "fr" };
            var roleList = new List<string[]>() { 
                new[] { "BUYER" }, 
                new[] { "TENANT" }, 
                new[] { "SELLER" },
                new[] { "LESSOR" },
                new[] { "BUYER", "BUYER" },
                new[] { "SELLER", "LESSOR" }
            };

            var password = "@Abcdefgh0";
            try
            {
                var randomValue = random.Next(150, 250);
                for (int x = 0; x < randomValue; x++)
                {
                    var _faker = new Faker();
                    var _userApplication = new ApplicationUser();
                    var role = _faker.PickRandom(roleList);
                    _userApplication.Gender = _faker.PickRandom(genderList);
                    _userApplication.Language = _faker.PickRandom(languageList);


                    if (_userApplication.Gender.Equals("F"))
                    {
                        _userApplication.FirstName = _faker.Name.FirstName(Bogus.DataSets.Name.Gender.Female);
                        _userApplication.LastName = _faker.Name.LastName(Bogus.DataSets.Name.Gender.Female);
                    }
                    else
                    {
                        _userApplication.FirstName = _faker.Name.FirstName(Bogus.DataSets.Name.Gender.Male);
                        _userApplication.LastName = _faker.Name.LastName(Bogus.DataSets.Name.Gender.Male);
                    }
                    _userApplication.Email = _faker.Internet.Email(_userApplication.FirstName, _userApplication.LastName);
                    _userApplication.UserName = _userApplication.Email;
                    _userApplication.DateOfBirth = _faker.Person.DateOfBirth;
                    _userApplication.CreatedAt = DateTime.Now;
                    _userApplication.UpdatedAt = DateTime.Now;
                    _userApplication.EmailConfirmed = true;
                    _userApplication.PhoneNumber = _faker.Phone.PhoneNumberFormat(0);
                    _userApplication.PhoneNumberConfirmed = true;
                    _userApplication.Biography = _faker.Lorem.Paragraph(100);
                    _userApplication.Avatar = _faker.Internet.Avatar();

                    var result = _userManager.CreateAsync(_userApplication, password).Result;
                    Task.Delay(1000).Wait();
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRolesAsync(_userApplication, role);
                    }
                    
                }
                _logger.LogInformation($"profile job done success ! {randomValue} entries");
            }
            catch (Exception e)
            {
                _logger.LogWarning("profile job done warning error catch " + e);
            }
            
        }
    }
}
