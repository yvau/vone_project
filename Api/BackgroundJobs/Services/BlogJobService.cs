﻿using Bogus;
using Hangfire;
using BackgroundJobs.Data;
using BackgroundJobs.Helpers;
using BackgroundJobs.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BackgroundJobs.Services
{
    public class BlogJobService : IJob
    {
        private readonly IAutoIncrementInterface _autoIncrementInterface;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly ILogger<BlogJobService> _logger;
        private static Random random = new Random();

        public BlogJobService(IAutoIncrementInterface autoIncrementInterface, 
                              UserManager<ApplicationUser> userManager,
                              ILogger<BlogJobService> logger,
                              ApplicationDbContext applicationDbContext) 
        {
            _userManager = userManager;
            _autoIncrementInterface = autoIncrementInterface;
            _applicationDbContext = applicationDbContext;
            _logger = logger;
        }

        public async Task Run(IJobCancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            await Create();
        }
        public async Task Create()
        {
            try
            {
                var randomValue = random.Next(20, 50);

                for (int i = 0; i < randomValue; i++)
                {
                   var _profiles =  _userManager.Users.Where(b => b.Blog.Count() <= 12).ToList();
                    int index = random.Next(_profiles.Count);

                    var tagList = new[] { "BUSINESS",
                                   "ANALYST",
                                   "LIFESTYLE",
                                   "DECORATION",
                                   "NEWS",
                                   "SOCIAL MEDIA",
                                   "DIARY",
                                   "FINANCE",
                                   "TRAVEL",
                                   "ENTERTAINMENT",
                                   "PARENTING",
                                   "PETS",
                                   "SPORTS",
                                   "MUSIC",
                                   "DIY"};

                    var _faker = new Faker();
                    var _blogs = new Blog();

                    _blogs.Id = _autoIncrementInterface.GetId(Resources.BLOG);
                    _blogs.Title = _faker.Lorem.Sentence();
                    _blogs.TitleSlug = _faker.Lorem.Slug();
                    _blogs.ImagePreviewUrl = _faker.Image.PicsumUrl(640, 480);
                    _blogs.Body = _faker.Lorem.Paragraphs(3);
                    _blogs.IsPublished = true;
                    _blogs.CreatedAt = DateTime.Now;
                    _blogs.UpdatedAt = DateTime.Now;
                    _blogs.Tags = _faker.PickRandom(tagList);
                    _blogs.Aspnetusers = _profiles[index];
                    _blogs.AspnetusersId = _profiles[index].Id;
                    _blogs.Enabled = true;

                    _applicationDbContext.Add(_blogs);
                    _applicationDbContext.SaveChanges();
                }

                _logger.LogInformation($"blog job done success ! {randomValue} entries");
            }
            catch (Exception e)
            {
                _logger.LogWarning("blog job done warning error catch " + e);
            }
        }
    }
}
