﻿using System;
using System.Collections.Generic;

namespace BackgroundJobs.Models
{
    public partial class LocationHasProposal
    {
        public long Id { get; set; }
        public long LocationId { get; set; }
        public long RequestProposalId { get; set; }

        public virtual Location Location { get; set; }
        public virtual RequestProposal RequestProposal { get; set; }
    }
}
