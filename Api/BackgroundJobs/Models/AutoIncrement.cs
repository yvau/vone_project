﻿using System;
using System.Collections.Generic;

namespace BackgroundJobs.Models
{
    public partial class AutoIncrement
    {
        public string Id { get; set; }
        public long? IncrementNumber { get; set; }
    }
}
