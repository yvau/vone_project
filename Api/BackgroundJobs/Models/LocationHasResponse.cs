﻿using System;
using System.Collections.Generic;

namespace BackgroundJobs.Models
{
    public partial class LocationHasResponse
    {
        public long Id { get; set; }
        public long LocationId { get; set; }
        public long ResponseProposalId { get; set; }

        public virtual Location Location { get; set; }
        public virtual ResponseProposal ResponseProposal { get; set; }
    }
}
