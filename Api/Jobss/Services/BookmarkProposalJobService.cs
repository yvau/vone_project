﻿using Bogus;
using Hangfire;
using Jobs.Data;
using Jobs.Helpers;
using Jobs.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Jobs.Services
{
    public class BookmarkProposalJobService : IJob
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _applicationDbContext;
        private static Random random = new Random();

        public BookmarkProposalJobService(UserManager<ApplicationUser> userManager,
                              ApplicationDbContext applicationDbContext) 
        {
            _userManager = userManager;
            _applicationDbContext = applicationDbContext;
        }

        public async Task Run(IJobCancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            await Create();
        }
        public async Task Create()
        {
            var roleList = new[] { "SELLER", "LESSOR" };

            try
            {
                for (int x = 0; x < random.Next(100); x++)
                {
                    var _faker = new Faker();

                    var _usersInRole = await _userManager.GetUsersInRoleAsync(_faker.PickRandom(roleList));
                    var _proposals = new List<RequestProposal>();

                    if (_faker.PickRandom(roleList).Equals("SELLER"))
                    {
                        _proposals = _applicationDbContext.RequestProposal.Where(r => r.TypeOfProposal == "FOR_SALE").ToList();
                    }
                    else 
                    { 
                        _proposals = _applicationDbContext.RequestProposal.Where(r => r.TypeOfProposal == "FOR_RENT").ToList();
                    }
                    
                    int indexProfile = random.Next(_usersInRole.Count);
                    int indexProposal = random.Next(_proposals.Count);

                    BookmarkProposal _bookmarkProposal = new BookmarkProposal();
                    _bookmarkProposal.RequestProposalId = _proposals[indexProposal].Id;
                    _bookmarkProposal.AspnetusersId = _usersInRole[indexProfile].Id;
                    _bookmarkProposal.CreatedAt = DateTime.Now;


                    _applicationDbContext.Add(_bookmarkProposal);
                    await _applicationDbContext.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"The exception {e}");
            }
        }
    }
}
