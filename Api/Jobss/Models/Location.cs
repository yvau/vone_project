﻿using System;
using System.Collections.Generic;

namespace Jobs.Models
{
    public partial class Location
    {
        public Location()
        {
            LocationHasProposal = new HashSet<LocationHasProposal>();
            LocationHasResponse = new HashSet<LocationHasResponse>();
            Property = new HashSet<Property>();
        }

        public long Id { get; set; }
        public string PostalCode { get; set; }
        public string Street { get; set; }
        public string CountryId { get; set; }
        public string ProvinceId { get; set; }
        public long CityId { get; set; }

        public virtual City City { get; set; }
        public virtual Country Country { get; set; }
        public virtual Province Province { get; set; }
        public virtual ICollection<LocationHasProposal> LocationHasProposal { get; set; }
        public virtual ICollection<LocationHasResponse> LocationHasResponse { get; set; }
        public virtual ICollection<Property> Property { get; set; }
    }
}
