﻿using System;
using System.Collections.Generic;

namespace Jobs.Models
{
    public partial class RequestHasResponseSatusLog
    {
        public RequestHasResponseSatusLog()
        {
            InverseRequestHasResponseSatusLogNavigation = new HashSet<RequestHasResponseSatusLog>();
        }

        public long Id { get; set; }
        public long RequestHasResponseSatusLogId { get; set; }
        public string Status { get; set; }
        public string Verb { get; set; }
        public DateTime? CreatedAt { get; set; }
        public long RequestHasResponseId { get; set; }
        public string AspnetusersId { get; set; }
        public string AspnetusersIdRecipient { get; set; }

        public virtual ApplicationUser Aspnetusers { get; set; }
        public virtual ApplicationUser AspnetusersIdRecipientNavigation { get; set; }
        public virtual RequestHasResponse RequestHasResponse { get; set; }
        public virtual RequestHasResponseSatusLog RequestHasResponseSatusLogNavigation { get; set; }
        public virtual ICollection<RequestHasResponseSatusLog> InverseRequestHasResponseSatusLogNavigation { get; set; }
    }
}
