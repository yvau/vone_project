﻿using System;
using System.Collections.Generic;

namespace Jobs.Models
{
    public partial class Property
    {
        public Property()
        {
            BookmarkProperty = new HashSet<BookmarkProperty>();
            PropertyPhoto = new HashSet<PropertyPhoto>();
            RequestHasResponse = new HashSet<RequestHasResponse>();
        }

        public long Id { get; set; }
        public string Type { get; set; }
        public string SaleType { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public string Bedrooms { get; set; }
        public string Bathrooms { get; set; }
        public long? YearBuilt { get; set; }
        public string Characteristics { get; set; }
        public string Status { get; set; }
        public string Size { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public long LocationId { get; set; }
        public string AspnetusersId { get; set; }

        public virtual ApplicationUser Aspnetusers { get; set; }
        public virtual Location Location { get; set; }
        public virtual ICollection<BookmarkProperty> BookmarkProperty { get; set; }
        public virtual ICollection<PropertyPhoto> PropertyPhoto { get; set; }
        public virtual ICollection<RequestHasResponse> RequestHasResponse { get; set; }
    }
}
