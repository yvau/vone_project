﻿using System;
using System.Collections.Generic;

namespace Jobs.Models
{
    public partial class BookmarkProposal
    {
        public DateTime? CreatedAt { get; set; }
        public long RequestProposalId { get; set; }
        public string AspnetusersId { get; set; }

        public virtual ApplicationUser Aspnetusers { get; set; }
        public virtual RequestProposal RequestProposal { get; set; }
    }
}
