﻿using Jobs.Data;
using Jobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobs.Services
{
    public interface IAutoIncrementInterface
    {
        long GetId(String resources);
    }

    public class AutoIncrementService : IAutoIncrementInterface
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public AutoIncrementService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        // manual increment id
        public long GetId(String resource)
        {
            // get the autoincrement object based on the parameter {resource}
            var autoIncrement = _applicationDbContext.AutoIncrement.FirstOrDefault(m => m.Id == resource);

            // assign 0 to the incrementNumber to return
            long incrementNumber = 0;

            // if autoincrement doesn't exist create the data
            if (autoIncrement == null)
            {
                AutoIncrement incrementIfNull = new AutoIncrement { Id = resource, IncrementNumber = ++incrementNumber };
                _applicationDbContext.Add(incrementIfNull);
                _applicationDbContext.SaveChanges();
            }

            // if autoincrement doesn't exist update the data add +1 to the IncrementNumber
            else
            {
                incrementNumber = autoIncrement.IncrementNumber.Value;
                autoIncrement.Id = resource;
                autoIncrement.IncrementNumber = ++incrementNumber;

                _applicationDbContext.Update(autoIncrement);
                _applicationDbContext.SaveChanges();
            }

            return incrementNumber;
        }

    }
}
