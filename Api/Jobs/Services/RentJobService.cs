﻿using Bogus;
using Hangfire;
using Jobs.Data;
using Jobs.Helpers;
using Jobs.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Jobs.Services
{
    public class RentJobService : IJob
    {
        private readonly IAutoIncrementInterface _autoIncrementInterface;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly ILogger<RentJobService> _logger;
        private static Random random = new Random();

        public RentJobService(IAutoIncrementInterface autoIncrementInterface, 
                              UserManager<ApplicationUser> userManager,
                              ILogger<RentJobService> logger,
                              ApplicationDbContext applicationDbContext) 
        {
            _userManager = userManager;
            _autoIncrementInterface = autoIncrementInterface;
            _applicationDbContext = applicationDbContext;
            _logger = logger;
        }

        public async Task Run(IJobCancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            await Create();
        }
        public async Task Create()
        {
            try
            {
                var randomValue = random.Next(50, 100);

                for (int i = 0; i < randomValue; i++)
                {
                    var _cities = _applicationDbContext.City.Include(p => p.Province).ThenInclude(c => c.Country).Where(r => r.ProvinceId.Equals("CA.10")).ToList();
                    var _cityCount = _cities.Count();
                    int _locationToTake = random.Next(1, 7);
                    var _location = _cities.Skip(random.Next(0, _cityCount)).Take(_locationToTake).ToList();

                    var _usersInRole = await _userManager.GetUsersInRoleAsync("TENANT");
                    var _profiles = _usersInRole.Where(r => r.RequestProposal.Count(r => r.IsEnabled.Equals("ENABLED") && r.TypeOfProposal.Equals("FOR_RENT")) < 4).ToList();
                    int index = random.Next(_profiles.Count);

                    var typeOfPropertyList = new[] { "ONEANDHALF",
                                                  "TWOANDHALF",
                                                  "THREEANDHALF",
                                                  "FOURANDHALF",
                                                  "FIVEANDHALF",
                                                  "SIXANDHALF",
                                                  "SEVENANDHALF",
                                                  "EIGHTANDHALF",
                                                  "NINEANDHALF",

                                                  "ONEANDHALF,TWOANDHALF",
                                                  "THREEANDHALF,FOURANDHALF,FIVEANDHALF",
                                                  "SIXANDHALF,SEVENANDHALF,EIGHTANDHALF,NINEANDHALF"};

                    var bankList = new[] { "TD Canada Trust",
                                       "Scotiabank",
                                       "BMO",
                                       "CIBC"
                    };

                    var _faker = new Faker();
                    RequestProposal _requestProposal = new RequestProposal();
                    _requestProposal.Id = _autoIncrementInterface.GetId(Resources.REQUESTPROPOSAL);
                    _requestProposal.TypeOfProperties = _faker.PickRandom(typeOfPropertyList);

                    _requestProposal.NumberOfParking = _faker.Random.Int(1, 3);
                    _requestProposal.NumberOfGarage = _faker.Random.Int(1, 3);
                    _requestProposal.IsWithPool = _faker.PickRandom(new[] { "IS_WITH_POOL",
                                                                                    "IS_NOT_WITH_POOL",
                                                                                    "IS_ANY_WITH_POOL"});
                    _requestProposal.IsFurnished = _faker.PickRandom(new[] { "IS_FURNISHED",
                                                                                     "IS_NOT_FURNISHED",
                                                                                     "IS_ANY_FURNISHED"});
                    _requestProposal.Size = _faker.Random.Decimal(1, 6000);
                    _requestProposal.TypeOfProposal = "FOR_RENT";
                    _requestProposal.PriceMinimum = _faker.Random.Decimal(0, 15000);
                    _requestProposal.PriceMaximum = _faker.Random.Decimal(15000, 15000000);
                    _requestProposal.IsThereContingency = _faker.Random.Bool();
                    _requestProposal.Urgency = _faker.PickRandom(new[] { "IS_URGENT",
                                                                             "IS_NOT_URGENT",
                                                                             "IS_ANY_URGENT"});
                    _requestProposal.IsEnabled = "ENABLED";
                    _requestProposal.IsPublished = "IS_PUBLISHED";
                    _requestProposal.Elevator = _faker.Random.Bool().ToString();
                    _requestProposal.Wheelchair = _faker.Random.Bool().ToString();
                    _requestProposal.NearbySchool = _faker.PickRandom(new[] { "IS_NEARBY_SCHOOL",
                                                                                    "IS_NOT_NEARBY_SCHOOL",
                                                                                    "IS_ANY_NEARBY_SCHOOL"});
                    _requestProposal.NearbyParc = _faker.PickRandom(new[] { "IS_NEARBY_PARC",
                                                                                    "IS_NOT_NEARBY_PARC",
                                                                                    "IS_ANY_NEARBY_PARC"});
                    _requestProposal.NearbySportsCenter = _faker.PickRandom(new[] { "IS_NEARBY_SPORT_CENTER",
                                                                                    "IS_NOT_NEARBY_SPORT_CENTER",
                                                                                    "IS_ANY_NEARBY_SPORT_CENTER"});
                    _requestProposal.NearbyTrade = _faker.PickRandom(new[] { "IS_NEARBY_TRADE",
                                                                                    "IS_NOT_NEARBY_TRADE",
                                                                                    "IS_ANY_NEARBY_TRADE"});
                    _requestProposal.NearbyPublicTransport = _faker.PickRandom(new[] { "IS_NEARBY_PUBLIC_TRANSPORT",
                                                                                    "IS_NOT_NEARBY_PUBLIC_TRANSPORT",
                                                                                    "IS_ANY_NEARBY_PUBLIC_TRANSPORT"});
                    _requestProposal.NearbyWaterfront = _faker.PickRandom(new[] { "IS_NEARBY_WATER_FRONT",
                                                                                    "IS_NOT_NEARBY_WATER_FRONT",
                                                                                    "IS_ANY_NEARBY_WATER_FRONT"});
                    _requestProposal.NearbyNavigableWaterBody = _faker.PickRandom(new[] { "IS_NEARBY_NAVIGABLE_WATER_BODY",
                                                                                    "IS_NOT_NEARBY_NAVIGABLE_WATER_BODY",
                                                                                    "IS_ANY_NEARBY_NAVIGABLE_WATER_BODY"});
                    _requestProposal.Status = "ENABLED";
                    _requestProposal.AgeOfProperty = _faker.PickRandom(new[] { "IS_AGE_NEW",
                                                                                   "IS_AGE_NOT_NEW",
                                                                                   "IS_ANY_AGE"});
                    _requestProposal.CreatedAt = DateTime.Now;
                    _requestProposal.UpdatedAt = DateTime.Now;
                    _requestProposal.AspnetusersId = _profiles[index].Id;
                    _requestProposal.Aspnetusers = _profiles[index];


                    // We init the location has proposal
                    for(int j = 0; j < _location.Count(); j++)
                    {
                        var _fakerLocationHasProposals = new Faker(); // default en
                        var locationHasProposalId = _autoIncrementInterface.GetId(Resources.LOCATIONHASPROPOSAL);
                        var location = _autoIncrementInterface.GetId(Resources.LOCATION);

                        var _locationHasProposal = new LocationHasProposal
                        {
                            Id = locationHasProposalId,
                            Location = new Location
                            {
                                Id = location,
                                PostalCode = _fakerLocationHasProposals.Address.ZipCode(_location[j].Province.CountryId),
                                Street = _fakerLocationHasProposals.Address.StreetAddress(),
                                Country = _location[j].Province.Country,
                                Province = _location[j].Province,
                                City = _location[j]
                            },
                            RequestProposal = _requestProposal
                        };
                        _applicationDbContext.Add(_locationHasProposal);
                        _applicationDbContext.SaveChanges();
                    }
                    
                }

                _logger.LogInformation($"rent job done success ! {randomValue} entries");
            }
            catch (Exception e)
            {
                _logger.LogWarning($"rent job done warning error catch {e}");
            }
        }
    }
}
