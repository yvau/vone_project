﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobs.Helpers
{
    public class Resources
    {
        // DATA MODEL
        public static String BLOG = "blog";
        public static String REQUESTPROPOSAL = "requestproposal";
        public static String LOCATIONHASPROPOSAL = "locationhasproposal";
        public static String LOCATION = "location";
        public static String PROPERTYPHOTO = "propertyphoto";
    }
}
