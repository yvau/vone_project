﻿using System;
using System.Collections.Generic;

namespace Jobs.Models
{
    public partial class RequestProposal
    {
        public RequestProposal()
        {
            BookmarkProposal = new HashSet<BookmarkProposal>();
            LocationHasProposal = new HashSet<LocationHasProposal>();
            RequestHasResponse = new HashSet<RequestHasResponse>();
        }

        public long Id { get; set; }
        public int? Bathrooms { get; set; }
        public int? Bedrooms { get; set; }
        public decimal? Size { get; set; }
        public int? NumberOfParking { get; set; }
        public int? NumberOfGarage { get; set; }
        public string TypeOfProposal { get; set; }
        public decimal? PriceMinimum { get; set; }
        public decimal? PriceMaximum { get; set; }
        public bool? IsCurrentlyOwner { get; set; }
        public bool? IsThereContingency { get; set; }
        public string IsWithPool { get; set; }
        public string IsPreApproved { get; set; }
        public string BankingInstitution { get; set; }
        public string Urgency { get; set; }
        public string IsEnabled { get; set; }
        public string IsPublished { get; set; }
        public string IsFirstBuyer { get; set; }
        public string IsFurnished { get; set; }
        public string Elevator { get; set; }
        public string Wheelchair { get; set; }
        public string NearbySchool { get; set; }
        public string NearbyParc { get; set; }
        public string NearbySportsCenter { get; set; }
        public string NearbyTrade { get; set; }
        public string NearbyPublicTransport { get; set; }
        public string NearbyWaterfront { get; set; }
        public string NearbyNavigableWaterBody { get; set; }
        public string TypeOfProperties { get; set; }
        public string Status { get; set; }
        public string AgeOfProperty { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string AspnetusersId { get; set; }

        public virtual ApplicationUser Aspnetusers { get; set; }
        public virtual ICollection<BookmarkProposal> BookmarkProposal { get; set; }
        public virtual ICollection<LocationHasProposal> LocationHasProposal { get; set; }
        public virtual ICollection<RequestHasResponse> RequestHasResponse { get; set; }
    }
}
