﻿using System;
using System.Collections.Generic;

namespace Jobs.Models
{
    public partial class BookmarkProperty
    {
        public DateTime? CreatedAt { get; set; }
        public long PropertyId { get; set; }
        public string AspnetusersId { get; set; }

        public virtual ApplicationUser Aspnetusers { get; set; }
        public virtual Property Property { get; set; }
    }
}
