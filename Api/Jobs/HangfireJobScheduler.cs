﻿using Hangfire;
using Jobs.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobs
{
    public class HangfireJobScheduler
    {
        public static void SchedulerRecurringJobs()
        {
            RecurringJob.AddOrUpdate<ProfileJobService>(nameof(ProfileJobService),
              job => job.Run(JobCancellationToken.Null),
              "30 */4 * * *", TimeZoneInfo.Local); // "0 */6 * * *"

            RecurringJob.AddOrUpdate<BlogJobService>(nameof(BlogJobService),
              job => job.Run(JobCancellationToken.Null),
              "0 */5 * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate<BuyJobService>(nameof(BuyJobService),
              job => job.Run(JobCancellationToken.Null),
              "30 */6 * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate<RentJobService>(nameof(RentJobService),
              job => job.Run(JobCancellationToken.Null),
              "0 */6 * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate<BookmarkProposalJobService>(nameof(BookmarkProposalJobService),
              job => job.Run(JobCancellationToken.Null),
              "40 12 * * *", TimeZoneInfo.Local); // "0 0 * * *"

            RecurringJob.AddOrUpdate<BookmarkPropertyJobService>(nameof(BookmarkPropertyJobService),
              job => job.Run(JobCancellationToken.Null),
              "30 12 * * *", TimeZoneInfo.Local); // "0 0 * * *"

            RecurringJob.AddOrUpdate<PropertyForSaleJobService>(nameof(PropertyForSaleJobService),
              job => job.Run(JobCancellationToken.Null),
              "0 */5 * * *", TimeZoneInfo.Local); // "0 0 * * *"

            RecurringJob.AddOrUpdate<PropertyForRentJobService>(nameof(PropertyForRentJobService),
              job => job.Run(JobCancellationToken.Null),
              "5 */5 * * *", TimeZoneInfo.Local);


        }
    }
}
