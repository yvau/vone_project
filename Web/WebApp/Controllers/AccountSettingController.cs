﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Helpers;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Route("account/setting/")]
    public class AccountSettingController : Controller
    {
        private readonly ILogger<AccountSettingController> _logger;
        private readonly UtilService _utilService;

        public AccountSettingController(ILogger<AccountSettingController> logger,
                              UtilService utilService)
        {
            _logger = logger;
            _utilService = utilService;
        }

        [HttpGet("information/")]
        public async Task<IActionResult> InformationAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("billing/")]
        public async Task<IActionResult> BillingAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("notification/")]
        public async Task<IActionResult> NotificationAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("language/")]
        public async Task<IActionResult> LanguageAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("twofactor/")]
        public async Task<IActionResult> TwofactorAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("password/")]
        public async Task<IActionResult> PasswordAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("role/")]
        public async Task<IActionResult> RoleAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
