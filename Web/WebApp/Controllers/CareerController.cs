﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Helpers;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Route("career/")]
    public class CareerController : Controller
    {
        private readonly ILogger<CareerController> _logger;
        private readonly UtilService _utilService;

        public CareerController(ILogger<CareerController> logger,
                              UtilService utilService
            )
        {
            _logger = logger;
            _utilService = utilService;
        }

        [HttpGet]
        public async Task<IActionResult> IndexAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("{id}/")]
        public async Task<IActionResult> ShowAsync(long id)
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

    }
}
