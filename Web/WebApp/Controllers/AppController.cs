﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Helpers;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Route("app/")]
    public class AppController : Controller
    {
        private readonly ILogger<AppController> _logger;
        private readonly UtilService _utilService;

        public AppController(ILogger<AppController> logger,
                              UtilService utilService)
        {
            _logger = logger;
            _utilService = utilService;
        }

        [HttpGet("home/")]
        public async Task<IActionResult> HomeAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("dashboard/")]
        public async Task<IActionResult> DashboardAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
