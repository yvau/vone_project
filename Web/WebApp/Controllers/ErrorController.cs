﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApp.Controllers
{
    [Route("error/")]
    public class ErrorController : Controller
    {
        private readonly ILogger<ErrorController> _logger;

        public ErrorController(ILogger<ErrorController> logger)
        {
            _logger = logger;
        }

        [HttpGet("{errCode}")]
        public IActionResult Errors(string errCode)
        {
            if (errCode == "500" | errCode == "404")
            {
                return View($"~/Views/Error/{errCode}.cshtml");
            }

            return View("~/Views/Shared/Error.cshtml");
        }

    }
}
