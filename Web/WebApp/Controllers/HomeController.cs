﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Helpers;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Route("/")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UtilService _utilService;

        public HomeController(ILogger<HomeController> logger,
                              UtilService utilService
            )
        {
            _logger = logger;
            _utilService = utilService;
        }

        [HttpGet]
        public async Task<IActionResult> IndexAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("about/")]
        public async Task<IActionResult> AboutAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("concept/")]
        public async Task<IActionResult> ConceptAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("advantage/")]
        public async Task<IActionResult> AdvantageAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("relationship/")]
        public async Task<IActionResult> RelationshipAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilService.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
