const path               = require('path')
const dirApp             = path.join(__dirname, 'ClientApp/src')
const webpack            = require('webpack')
const RemovePlugin       = require('remove-files-webpack-plugin')
const nodeExternals      = require('webpack-node-externals')
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin')
const CopyPlugin         = require('copy-webpack-plugin')
const purgecss           = require('@fullhuman/postcss-purgecss')
// const PurgecssPlugin     = require('purgecss-webpack-plugin')

const TARGET_FRONT = process.env.WEBPACK_TARGET === 'front'
const TARGET_NODE = process.env.WEBPACK_TARGET === 'node'
const target = TARGET_NODE ? 'server' : 'client'
const jsFolder = TARGET_NODE ? '' : 'js/'

module.exports = {
  chainWebpack: (config) => {
    if (TARGET_FRONT) {
      config
        .plugin('html')
        .tap(args => {
          args[0].template = path.resolve(__dirname, 'ClientApp/public') + '/index.html', // template file
            args[0].filename = path.resolve(__dirname, 'Views/Shared') + '/_Layout.cshtml', // output file
            args[0].inject = false,
            args[0].minify = true
          return args
        })

      config.plugins.delete('preload')
      config.plugins.delete('prefetch')
    }
  },
    css: {
        extract: process.env.NODE_ENV === 'production' ? {
            filename: 'css/[name].css',
            chunkFilename: 'css/[name].css'
        } : false
    },
    publicPath: '/',
    productionSourceMap: false,
    outputDir: TARGET_NODE ? path.join(__dirname, 'render') : path.join(__dirname, 'wwwroot'),
    configureWebpack: {
        entry: `./ClientApp/src/entry-${target}`,
        target: TARGET_NODE ? 'node' : 'web',
        output: {
            libraryTarget: TARGET_NODE ? 'commonjs2' : undefined,
            filename: TARGET_NODE ? `${jsFolder}[name].js` : `${jsFolder}[name].[hash].js`,
            chunkFilename: TARGET_NODE ? `${jsFolder}[name].js` : `${jsFolder}[name].[hash].js`
        },
      plugins: [
      ].concat(TARGET_NODE ? [
        new webpack.optimize.LimitChunkCountPlugin({
          maxChunks: 1
        }),
        new webpack.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
          'process.env.VUE_ENV': '"server"'
        }),
        new RemovePlugin({
          after: {
            root: './render',
            include: [
              './img',
              './favicon.ico',
              './index.html'
            ]
          },
        })
      ] : [
          new VueSSRClientPlugin(),
          //Only add purgecss in production
          new RemovePlugin({
            after: {
              include: [
                // 'wwwroot/index.html'
              ]
            }
          }),
          new CopyPlugin({
            patterns: [
              {
                from: path.resolve(__dirname, 'ClientApp/public/favicon.ico'),
                to: path.resolve(__dirname, 'wwwroot')
              }
            ],
          })
        ].concat(process.env.NODE_ENV === 'production' ? [
          new purgecss({
            content: [
              path.resolve(__dirname, dirApp + 'views/**/*.vue'),
              path.resolve(__dirname, dirApp + 'views/**/**/*.vue')
            ]
          })
          //new PurgecssPlugin({
            //paths: glob.sync([
              //path.join(__dirname, './src/index.html'),
              //path.join(__dirname, './src/**/*.vue'),
              //path.join(__dirname, './src/**/*.js')
            //])
          //})
        ] : [])),
        optimization: {
            splitChunks: TARGET_NODE ? undefined : {
                name: 'vendor',
                chunks: 'all',
                maxInitialRequests: Infinity,
                minSize: 0
            }
        },
        resolve: {
            alias: {
                '@': dirApp
            }
        },
        // https://webpack.js.org/configuration/externals/#function
        // https://github.com/liady/webpack-node-externals
        // Externalize app dependencies. This makes the server build much faster
        // and generates a smaller bundle file.
        externals: TARGET_NODE ? nodeExternals({
            allowlist: /\.css$/
        }) : undefined
    }
}
