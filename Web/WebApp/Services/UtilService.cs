﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace WebApp.Services
{
    public interface IUtilInterface
    {
        String SpaData(JToken prerender);
    }

    public class UtilService : IUtilInterface
    {

        public String SpaData(JToken prerender)
        {
            return "window.__INITIAL_STATE__ = " + prerender.ToString(Formatting.None);
        }

    }
}
