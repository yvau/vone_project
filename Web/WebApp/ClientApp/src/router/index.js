import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home/Index.vue')
  },
  {
    path: '/about/',
    name: 'About',
    component: () => import('@/views/Home/About.vue'),
    meta: {
      breadcrumb: [
        { name: 'Home', link: '/' },
        { name: 'About' }
      ]
    }
  },
  {
    path: '/concept/',
    name: 'Concept',
    component: () => import('@/views/Home/Concept.vue'),
    meta: {
      breadcrumb: [
        { name: 'Home', link: '/' },
        { name: 'Concept' }
      ]
    }
  },
  {
    path: '/advantage/',
    name: 'Advantage',
    component: () => import('@/views/Home/Advantage.vue'),
    meta: {
      breadcrumb: [
        { name: 'Home', link: '/' },
        { name: 'Advantage' }
      ]
    }
  },
  {
    path: '/relationship/',
    name: 'Relationship',
    component: () => import('@/views/Home/Relationship.vue'),
    meta: {
      breadcrumb: [
        { name: 'Home', link: '/' },
        { name: 'Relationship' }
      ]
    }
  },
  {
    path: '/contact/',
    name: 'Contact',
    component: () => import('@/views/Home/Contact.vue'),
    meta: {
      breadcrumb: [
        { name: 'Home', link: '/' },
        { name: 'Contact' }
      ]
    }
  },
  {
    path: '/blog/',
    component: () => import('@/views/Blog/Index.vue'),
    children: [
      {
        path: '',
        name: 'Blog.Home',
        component: () => import('@/views/Blog/Home.vue'),
        meta: {
          breadcrumb: [
            { name: 'Home', link: '/' },
            { name: 'Blog' }
          ]
        }
        // If the user needs to be a guest to view this page.
      },
      {
        path: ':id(\\d+)/',
        name: 'Blog.Show',
        component: () => import('@/views/Blog/Show.vue'),
        meta: {
          breadcrumb: [
            { name: 'Home', link: '/' },
            { name: 'Blog', link: '/blog/' },
            { name: 'fef' }
          ]
        }
        // If the user needs to be a guest to view this page.
      },
      {
        path: 'new/',
        name: 'Blog.New',
        component: () => import('@/views/Blog/New.vue')
        // If the user needs to be a guest to view this page.
      },
      {
        path: 'list/',
        name: 'Blog.List',
        component: () => import('@/views/Blog/List.vue')
        // If the user needs to be a guest to view this page.
      }
    ]
  },
  {
    path: '/career/',
    component: () => import('@/views/Career/Index.vue'),
    children: [
      {
        path: '',
        name: 'Career.Home',
        component: () => import('@/views/Career/Home.vue')
      },
      {
        path: ':id(\\d+)/',
        name: 'Career.Show',
        component: () => import('@/views/Career/Show.vue')
      }
    ]
  },
  {
    path: '/account/',
    component: () => import('@/views/Account/Index.vue'),
    children: [
      {
        path: ':id(\\d+)/',
        name: 'Account.Show',
        component: () => import('@/views/Account/Show.vue')
        // If the user needs to be a guest to view this page.
      },
      {
        path: 'complete/',
        component: () => import('@/views/Account/Complete/Index.vue'),
        children: [
          {
            path: 'role/',
            name: 'Account.Complete.Role',
            component: () => import('@/views/Account/Complete/Role.vue')
          },
          {
            path: 'information/',
            name: 'Account.Complete.Information',
            component: () => import('@/views/Account/Complete/Information.vue')
          },
          {
            path: 'summary/',
            name: 'Account.Complete.Summary',
            component: () => import('@/views/Account/Complete/Summary.vue')
          }
        ]
      },
      {
        path: 'setting/',
        component: () => import('@/views/Account/Setting/Index.vue'),
        children: [
          {
            path: 'information/',
            name: 'Account.Setting.Information',
            component: () => import('@/views/Account/Setting/Information.vue')
          },
          {
            path: 'password/',
            name: 'Account.Setting.Password',
            component: () => import('@/views/Account/Setting/Password.vue')
          },
          {
            path: 'twofactor/',
            name: 'Account.Setting.TwoFactor',
            component: () => import('@/views/Account/Setting/TwoFactor.vue')
          },
          {
            path: 'role/',
            name: 'Account.Setting.Role',
            component: () => import('@/views/Account/Setting/Role.vue')
          },
          {
            path: 'language/',
            name: 'Account.Setting.Language',
            component: () => import('@/views/Account/Setting/Language.vue')
          },
          {
            path: 'notification/',
            name: 'Account.Setting.Notification',
            component: () => import('@/views/Account/Setting/Notification.vue')
          },
          {
            path: 'billing/',
            name: 'Account.Setting.Billing',
            component: () => import('@/views/Account/Setting/Billing.vue')
          }
        ]
      }
    ]
  },
  {
    path: '/buy/',
    component: () => import('@/views/Buy/Index.vue'),
    children: [
      {
        path: 'new/',
        name: 'Buy.New',
        component: () => import('@/views/Buy/New.vue')
      },
      {
        path: ':id(\\d+)/',
        name: 'Buy.Show',
        component: () => import('@/views/Buy/Show.vue')
      },
      {
        path: ':id(\\d+)/edit/',
        name: 'Buy.Edit',
        component: () => import('@/views/Buy/Edit.vue')
      },
      {
        path: 'list/',
        name: 'Buy.List',
        component: () => import('@/views/Buy/List.vue')
      }
    ]
  },
  {
    path: '/rent/',
    component: () => import('@/views/Rent/Index.vue'),
    children: [
      {
        path: 'new/',
        name: 'Rent.New',
        component: () => import('@/views/Rent/New.vue')
      },
      {
        path: ':id(\\d+)/',
        name: 'Rent.Show',
        component: () => import('@/views/Rent/Show.vue')
      },
      {
        path: ':id(\\d+)/edit/',
        name: 'Rent.Edit',
        component: () => import('@/views/Rent/Edit.vue')
      },
      {
        path: 'list/',
        name: 'Rent.List',
        component: () => import('@/views/Rent/List.vue')
      }
    ]
  },
  {
    path: '/property/',
    component: () => import('@/views/Property/Index.vue'),
    children: [
      {
        path: 'new/',
        name: 'Property.New',
        component: () => import('@/views/Property/New.vue')
      },
      {
        path: ':id(\\d+)/',
        name: 'Property.Show',
        component: () => import('@/views/Property/Show.vue')
      },
      {
        path: ':id(\\d+)/edit/',
        name: 'Property.Edit',
        component: () => import('@/views/Property/Edit.vue')
      },
      {
        path: 'list/',
        name: 'Property.List',
        component: () => import('@/views/Property/List.vue')
      }
    ]
  },
  {
    path: '/sell/',
    component: () => import('@/views/Sell/Index.vue'),
    children: [
      {
        path: 'new/',
        name: 'Sell.New',
        component: () => import('@/views/Sell/New.vue')
      },
      {
        path: ':id(\\d+)/',
        name: 'Sell.Show',
        component: () => import('@/views/Sell/Show.vue')
      },
      {
        path: ':id(\\d+)/edit/',
        name: 'Sell.Edit',
        component: () => import('@/views/Sell/Edit.vue')
      },
      {
        path: 'list/',
        name: 'Sell.List',
        component: () => import('@/views/Sell/List.vue')
      }
    ]
  },
  {
    path: '/lease/',
    component: () => import('@/views/Lease/Index.vue'),
    children: [
      {
        path: 'new/',
        name: 'Lease.New',
        component: () => import('@/views/Lease/New.vue')
      },
      {
        path: ':id(\\d+)/',
        name: 'Lease.Show',
        component: () => import('@/views/Lease/Show.vue')
      },
      {
        path: ':id(\\d+)/edit/',
        name: 'Lease.Edit',
        component: () => import('@/views/Lease/Edit.vue')
      },
      {
        path: 'list/',
        name: 'Lease.List',
        component: () => import('@/views/Lease/List.vue')
      }
    ]
  },
  {
    path: '/app/',
    component: () => import('@/views/App/Index.vue'),
    children: [
      {
        path: '',
        redirect: 'home/'
      },
      {
        path: 'home/',
        name: 'App.Home',
        component: () => import('@/views/App/Home.vue')
      },
      {
        path: 'dashboard/',
        name: 'App.Dasboard',
        component: () => import('@/views/App/Dashboard.vue')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
