import '@/plugins/bootstrap'
import '@/plugins/stylesheet'
import '@/plugins/turbolinks'
import '@/plugins/component-client'

import {
  app,
  router,
  store
} from './main'

/* if (process.env.NODE_ENV !== 'development') {
  store.replaceState(__INITIAL_STATE__)
} */

router.onReady(() => {
  router.beforeResolve((to, from, next) => {
    const matched = router.getMatchedComponents(to)
    const prevMatched = router.getMatchedComponents(from)

    // compare two list of components from previous route and current route
    let diffed = false
    const activated = matched.filter((c, i) => {
      return diffed || (diffed = (prevMatched[i] !== c))
    })

    // if no new components loaded, do nothing
    if (!activated.length) {
      return next()
    }

    // NProgress.start()
    // for each newly loaded components, asynchorously load data to them
    Promise.all(activated.map(c => {
      if (c.asyncData) {
        return c.asyncData({ store, route: to })
      }
    })).then(() => {
    // NProgress.done()
      next()
    }).catch(next)
  })
  app.$mount('#app')
})
