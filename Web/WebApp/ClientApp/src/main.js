import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import '@/plugins/veevalidate'
import '@/plugins/multiselect'
import '@/plugins/fortawesome'
import { i18n } from '@/plugins/vue-i18n'
import '@/plugins/component-server'

Vue.config.productionTip = false

const app = new Vue({
  store,
  router,
  i18n,
  render: h => h(App)
})

export { app, router, store }
