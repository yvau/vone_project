/* ============
 * Fontawesome
 * ============
 *
 * The internet's most popular icon toolkit has been redesigned and built from scratch.
 * On top of this, features like icon font ligatures, an SVG framework,
 * official NPM packages for popular frontend libraries like React, and access to a new CDN.
 *
 * https://github.com/FortAwesome/Font-Awesome
 */

import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
// import { faCheckCircle as fasCheckCircle, faBath, faBed, faCar, faChevronDown, faChevronUp, faCreditCard, faCogs } from '@fortawesome/free-solid-svg-icons'
import { faAngleRight, faEye, faEyeSlash, faCheckCircle as fasCheckCircle } from '@fortawesome/free-solid-svg-icons'
// import { faCheckCircle, faShareSquare, faStar, faEnvelopeOpen, faBell } from '@fortawesome/free-regular-svg-icons'
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons'
// import { faCcVisa, faCcMastercard, faCcAmex, faCcDiscover, faCcDinersClub, faCcJcb, faFacebookSquare, faGooglePlusSquare } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

config.autoAddCss = false
// config.searchPseudoElements = true

library.add(faAngleRight, faCheckCircle, fasCheckCircle, faEyeSlash, faEye)
// library.add(faCheckCircle, fasCheckCircle, faShareSquare, faStar, faBath, faBed, faCar, faChevronDown, faChevronUp, faEnvelopeOpen, faCreditCard, faCcVisa, faCcMastercard, faCcAmex, faCcDiscover, faCcDinersClub, faCcJcb, faFacebookSquare, faGooglePlusSquare, faCogs, faBell)
Vue.component('font-awesome-icon', FontAwesomeIcon)
