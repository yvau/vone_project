/* ============
 * Vue component
 * ============
 *
 */
import Vue from 'vue'
import Breadcrumbs from '@/components/Breadcrumbs.vue'
import AppImage from '@/components/AppImage.vue'

Vue.component('bread-crumbs', Breadcrumbs)
Vue.component('app-image', AppImage)
