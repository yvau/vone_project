/* ============
 * Vue component
 * ============
 *
 */
import Vue from 'vue'
import QuillEditor from '@/components/QuillEditor.vue'

Vue.component('quill-editor', QuillEditor)
