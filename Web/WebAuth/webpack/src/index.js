// Test import of a JavaScript function, an SVG, and Sass
__webpack_nonce__ = '<%=nonce%>'

import './js/Pace'
import { LoadPaypal } from './js/Paypal'
import { CkEditor } from './js/CKEditorInterop'
import { LoadQrCode } from './js/QrCode'
import { Mask } from './js/IMask'
import { TinySlider } from './js/TinySlider'

// import { HelloWorld } from './js/HelloWorld'
// import WebpackLogo from './images/webpack-logo.svg'

import './js/StyleSheet'

export {
    LoadPaypal,
    LoadQrCode,
    Mask,
    CkEditor,
    TinySlider
}

// Create SVG logo node
/*const logo = document.createElement('img')
logo.src = WebpackLogo

// Create heading node
const greeting = document.createElement('h1')
greeting.textContent = HelloWorld()

// Append SVG and heading nodes to the DOM
const app = document.querySelector('#root')
app.append(logo, greeting)*/
