export const LoadPaypal = {
  createScriptLibrary: function () {
    const script = document.createElement("script")
    script.src =
        "https://www.paypalobjects.com/api/checkout.js"
    document.body.appendChild(script)
  },
  RenderPaypal: function () {
    return paypal.Button.render({
      // Configure environment
      env: 'sandbox',
      client: {
        sandbox: 'AQ6R03HMYZCz_GbObsHAm8isDOI4279vm4stppI6BD3D_nptgyf8UmSVJwNl-WhRtGMiWr8A7mtdUOSf',
        production: 'demo_production_client_id'
      },
      // Customize button (optional)
      locale: 'en_US',
      style: {
        size: 'small',
        color: 'gold',
        shape: 'pill',
      },
  
      // Enable Pay Now checkout flow (optional)
      commit: true,
  
      // Set up a payment
      payment: function(data, actions) {
        return actions.payment.create({
          transactions: [{
            amount: {
              total: '0.01',
              currency: 'USD'
            }
          }]
        });
      },
      // Execute the payment
      onAuthorize: function(data, actions) {
        return actions.payment.execute().then(function() {
          // Show a confirmation message to the buyer
          window.alert('Thank you for your purchase!');
        });
      }
    }, '#paypal-button');
  }
};
