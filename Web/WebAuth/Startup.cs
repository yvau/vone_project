using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WebAuth.Models;
using IdentityServer4;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebAuth;
using WebAuth.Data;
using WebAuth.Services;

namespace Vone.Auth
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            services.AddDbContext<ApplicationDbContext>(builder =>
                builder.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"), sqlOptions => sqlOptions.MigrationsAssembly(migrationsAssembly)));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

            services.AddControllersWithViews();

            services.ConfigureApplicationCookie(config =>
            {
                config.Cookie.Name = "IdentityServer.Cookie";
                config.LoginPath = "/login";
                config.LogoutPath = "/logout";
            });

            IIdentityServerBuilder ids = services.AddIdentityServer(options =>
            {
                options.Events.RaiseErrorEvents = true;
                options.Events.RaiseInformationEvents = true;
                options.Events.RaiseFailureEvents = true;
                options.Events.RaiseSuccessEvents = true;
            })
                .AddInMemoryApiScopes(Config.GetApiScopes())
                .AddInMemoryApiResources(Config.GetApis())
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryClients(Config.GetClients())
                .AddTestUsers(TestUsers.Users)
                .AddDeveloperSigningCredential(persistKey: false)
                .AddAspNetIdentity<ApplicationUser>();
                // .AddProfileService<ProfileService>();

            if (_env.IsDevelopment())
            {
                // Block 3:
                // Adding Developer Signing Credential, This will generate tempkey.rsa file 
                // In Production you need to provide the asymmetric key pair (certificate or rsa key) that support RSA with SHA256.
                ids.AddDeveloperSigningCredential();
            }
            else
            {
                throw new Exception("need to configure key material");
            }

            // in-memory client and scope stores
            /*ids.AddInMemoryClients(Clients.Get())
                .AddInMemoryIdentityResources(Resources.GetIdentityResources())
                .AddInMemoryApiResources(Resources.GetApiResources())
                // .AddTestUsers(TestUsers.Users)
                .AddInMemoryApiScopes(Resources.GetApiScopes());*/

            services.AddAuthentication()
                .AddFacebook(config => { /* Create App https://developers.facebook.com/apps */
                    config.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

                    config.AppId = Configuration.GetSection("Oauth")["Facebook:ClientId"];
                    config.AppSecret = Configuration.GetSection("Oauth")["Facebook:ClientSecret"];
                    config.SaveTokens = true;
                    config.Scope.Clear();
                    // Configuration.GetSection("oauth.facebook.Permissions").GetChildren().Each(x => options.Scope.Add(x.Value));
                })
                .AddGoogle(config => { /* Create App https://console.developers.google.com/apis/credentials */
                    config.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

                    config.ClientId = Configuration.GetSection("Oauth")["Google:ClientId"];
                    config.ClientSecret = Configuration.GetSection("Oauth")["Google:ClientSecret"];
                    config.SaveTokens = true;
                }).AddTwitter(config => { /* Create Twitter App at: https://dev.twitter.com/apps */
                    config.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

                    config.ConsumerKey = Configuration.GetSection("Oauth")["Twitter:ClientId"];
                    config.ConsumerSecret = Configuration.GetSection("Oauth")["Twitter:ClientSecret"]; ;
                    config.SaveTokens = true;
                    config.RetrieveUserDetails = true;
                });

            // EF client, scope, and persisted grant stores
            /*ids.AddOperationalStore(options =>
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(connectionString, sqlOptions => sqlOptions.MigrationsAssembly(migrationsAssembly)))
                .AddConfigurationStore(options =>
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(connectionString, sqlOptions => sqlOptions.MigrationsAssembly(migrationsAssembly)));*/

            // ASP.NET Identity integration

            // add service Microsoft.AspNetCore.NodeServices.INodeServices to have spa ssr
            // services.AddNodeServices();
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IRazorViewToStringRenderer, RazorViewToStringRenderer>();
            services.AddTransient<IProfileService, ProfileService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseIdentityServer();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
