﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAuth.Controllers
{
    [Route("api/account/")]
    [ApiController]
    public class CompleteController : ControllerBase
    {
        // GET: api/<CompleteController>
        [HttpPost("information/")]
        public IEnumerable<string> CompleteInformation()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<CompleteController>/5
        [HttpPost("role/")]
        public IEnumerable<string> CompleteRole()
        {
            return new string[] { "value1", "value2" };
        }
    }
}
