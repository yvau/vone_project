﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAuth.Models
{
    public class ApplicationUser : IdentityUser
    {
        [PersonalData]
        public string Avatar { get; set; }

        [PersonalData]
        public string FirstName { get; set; }

        [PersonalData]
        public string LastName { get; set; }

        [PersonalData]
        public string Gender { get; set; }

        [PersonalData]
        public DateTime DateOfBirth { get; set; }

        [PersonalData]
        public string Biography { get; set; }

        [PersonalData]
        public string Language { get; set; }

        public string FacebookHandler { get; set; }

        public string GoogleHandler { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
