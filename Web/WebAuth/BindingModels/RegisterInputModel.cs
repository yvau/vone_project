﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAuth.BindingModels
{
    public class RegisterInputModel
    {
        [Required]
        [EmailAddress]
        [Remote(action: "VerifyEmail", controller: "Account")]
        public string Email { get; set; }

        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "The Terms and conditions are required!")]
        public bool TermsAndConditions { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
