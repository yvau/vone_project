// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebAuth.BindingModels
{
    public class RegisterViewModel : RegisterInputModel
    {
        public string ReturnUrl { get; set; }
        public IList<AuthenticationScheme> ExternalProviders { get; set; }
    }
}