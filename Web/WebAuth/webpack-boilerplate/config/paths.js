const path = require('path')

module.exports = {
  // Source files
  src: path.resolve(__dirname, '../src'),

  // production build files
  tmpl: path.resolve(__dirname, '../../Views/Shared'), 

  // Production build files
  build: path.resolve(__dirname, '../../wwwroot'),

  // Static files that get copied to build folder
  public: path.resolve(__dirname, '../public'),
}
