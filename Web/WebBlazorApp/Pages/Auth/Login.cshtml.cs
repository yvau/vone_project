using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebBlazorApp.Services;

namespace WebBlazorApp.Pages.Auth
{
    public class LoginModel : PageModel
    {
        public readonly BlazorServerAuthStateCache Cache;

        public LoginModel(BlazorServerAuthStateCache cache)
        {
            Cache = cache;
        }

        public IActionResult OnGet()
        {
            System.Diagnostics.Debug.WriteLine("\n_Host OnGetLogin");

            var urlFromQuery = string.IsNullOrEmpty(Request.Query["returnUrl"]) ? Url.Content("~/") : new Uri(Request.Query["returnUrl"]).PathAndQuery;

            var authProps = new AuthenticationProperties
            {
                IsPersistent = true,
                ExpiresUtc = DateTimeOffset.UtcNow.AddHours(15),
                // ExpiresUtc = DateTimeOffset.UtcNow.AddSeconds(30),
                RedirectUri = urlFromQuery
            };
            return Challenge(authProps, "oidc");
        }
    }
}
