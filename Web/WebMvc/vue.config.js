const path = require('path')
const dirApp = path.join(__dirname, 'webpack/src')
const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')
// const WriteFilePlugin = require('write-file-webpack-plugin')
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin')

const TARGET_NODE = process.env.WEBPACK_TARGET === 'node'
const TARGET_FRONT = process.env.WEBPACK_TARGET === 'front'
const target = TARGET_NODE ? 'server' : 'client'
const jsFolder = TARGET_NODE ? '' : 'js/'

module.exports = {
    chainWebpack: (config) => {
        if (TARGET_FRONT) {
          config
            .plugin('html')
            .tap(args => {
                args[0].template = path.resolve(__dirname, 'webpack/public') + '/index.html', // template file
                args[0].filename = path.resolve(__dirname, 'Views/Shared') + '/_Layout.cshtml', // output file
                args[0].inject = false,
                args[0].minify = true
                return args
            })

           config.plugins.delete('preload')
           config.plugins.delete('prefetch')
        }
    },
    css: {
        extract: process.env.NODE_ENV === 'production' ? {
            filename: 'css/[name].[hash].css',
            chunkFilename: 'css/[name].[hash].css'
        } : false
    },
    publicPath: '/',
    productionSourceMap: false,
    outputDir: TARGET_NODE ? path.join(__dirname, 'webpack/render') : path.join(__dirname, 'wwwroot'),
    configureWebpack: {
        entry: `./webpack/src/entry-${target}`,
        target: TARGET_NODE ? 'node' : 'web',
        output: {
            libraryTarget: TARGET_NODE ? 'commonjs2' : undefined,
            filename: TARGET_NODE ? `${jsFolder}[name].js` : `${jsFolder}[name].[hash].js`,
            chunkFilename: `${jsFolder}[name].js`
        },
        plugins: [
            // new WriteFilePlugin(),
            TARGET_NODE ? new webpack.optimize.LimitChunkCountPlugin({
                maxChunks: 1
            }) : new VueSSRClientPlugin()
        ],
        /*optimization: {
            splitChunks: TARGET_NODE ? undefined : {
                name: 'vendor',
                chunks: 'all',
                maxInitialRequests: Infinity,
                minSize: 0
            }
        },*/
        resolve: {
            alias: {
                '@': dirApp
            }
        },
        // https://webpack.js.org/configuration/externals/#function
        // https://github.com/liady/webpack-node-externals
        // Externalize app dependencies. This makes the server build much faster
        // and generates a smaller bundle file.
        externals: TARGET_NODE ? nodeExternals({
            allowlist: /\.css$/
        }) : undefined
    },
    /*devServer: {
    proxy: {
      "^/api": {
        target: "https://localhost:5001",
        changeOrigin: true,
        logLevel: "debug",
        pathRewrite: { "^/api": "/" }
      }
    }
  }*/
}
