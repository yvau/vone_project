/* ============
 * Axios
 * ============
 *
 * Promise based HTTP client for the browser and node.js.
 * Because Vue Resource has been retired, Axios will now been used
 * to perform AJAX-requests.
 *
 * https://github.com/mzabriskie/axios
 */

import Vue from 'vue'
import Axios from 'axios'

// Axios.defaults.baseURL = 'https://localhost:5001'
Axios.defaults.headers.common.Accept = 'application/json'

// Bind Axios to Vue.
Vue.prototype.$http = Axios
