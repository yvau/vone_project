/* ============
 * Turbolinks
 * ============
 *
 * Turbolinks® makes navigating your web application faster.
 *
 * https://github.com/turbolinks/turbolinks
 */

var Turbolinks = require('turbolinks')
Turbolinks.start()

// Monkey patch Turbolinks to render 403, 404 & 500 normally
// See https://github.com/turbolinks/turbolinks/issues/179
Turbolinks.HttpRequest.prototype.requestLoaded = function () {
  return this.endRequest(function () {
    var code = this.xhr.status
    // 200 <= code
    // if ((code >= 200 && code < 300) || code === 403 || code === 404 || code === 500) {
    if ((code >= 200 && code < 300) || code === 403 || code === 404 || code === 500) {
      this.delegate.requestCompletedWithResponse(this.xhr.responseText, this.xhr.getResponseHeader('Turbolinks-Location'))
    } else {
      this.failed = true
      this.delegate.requestFailedWithStatusCode(code, this.xhr.responseText)
    }
  }.bind(this))
}
